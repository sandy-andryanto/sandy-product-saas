$(function(){

    if($("#table-data").length){
        var columns = [
            {
                "data": "id",
                "orderable": false,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'name',
            },
            {
                data: 'email',
            },
            {
                data: 'website',
            },
            {
                data: 'phone',
            },
            {
                data: 'address',
            },
            {
                data: 'action',
                "orderable": false,
                "className": "text-center"
            },
        ];
        setDataTable("#table-data", columns, "reference/contacts/datatable");
    }

    

   

});