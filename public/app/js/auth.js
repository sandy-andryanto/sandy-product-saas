$(document).ajaxComplete(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

$(function(){

    var base_url = $("body").attr("data-base-url");

    $('[data-toggle="tooltip"]').tooltip();

    if($("#captcha-section").length){
        $("#captcha-section img").addClass("img-thumbnail").addClass("img-responsive");
         $("#btn-reload-captcha").click(function(e){
             e.preventDefault();
             $.get(base_url+"/reload/captcha", function(result){
                 var elem = $(result).addClass("img-thumbnail").addClass("img-responsive");
                 $("#captcha-img").html(elem);
             });
             return false;
         });
     }
});