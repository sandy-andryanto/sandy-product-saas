$(function(){

    if($("#table-data").length){
        var columns = [
            {
                "data": "id",
                "orderable": false,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'created_at',
            },
            {
                data: 'instance',
            },
            {
                data: 'channel',
            },
            {
                data: 'level',
            },
            {
                data: 'level_name',
            },
            {
                data: 'message',
            },
            {
                data: 'context',
            }
        ];
        setDataTable("#table-data", columns, "configuration/logs/datatable");
    }

    

   

});