$(function(){

    if($("#table-data").length){
        var columns = [
            {
                "data": "id",
                "orderable": false,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'username',
            },
            {
                data: 'email',
            },
            {
                data: 'phone',
            },
            {
                data: 'roles',
                "orderable": false,
                "className": "text-center"
            },
            {
                data: 'action',
                "orderable": false,
                "className": "text-center"
            },
        ];
        setDataTable("#table-data", columns, "configuration/users/datatable");
    }

    

   

});