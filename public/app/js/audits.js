$(function(){

    if($("#table-data").length){
        var columns = [
            {
                "data": "id",
                "orderable": false,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'updated_at',
            },
            {
                data: 'username',
            },
            {
                data: 'event',
            },
            {
                data: 'url',
            },
            {
                data: 'ip_address',
            }
        ];
        setDataTable("#table-data", columns, "configuration/audits/datatable");
    }

    

   

});