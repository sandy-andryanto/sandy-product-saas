<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('phone_code')->nullable()->index();
            $table->string('lang_code')->nullable()->index();
            $table->string('code')->nullable()->index();
            $table->string('name')->nullable()->index();
            $table->decimal('latitude', 10, 8)->default(0)->index();
            $table->decimal('longitude', 11, 8)->default(0)->index();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('area_provinces', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('country_id')->nullable()->index();
            $table->string('country_code')->nullable()->index();
            $table->string('code')->nullable()->index();
            $table->string('name')->nullable()->index();
            $table->decimal('latitude', 10, 8)->default(0)->index();
            $table->decimal('longitude', 11, 8)->default(0)->index();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('area_regencies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('province_id')->nullable()->index();
            $table->string('province_code')->nullable()->index();
            $table->string('code')->nullable()->index();
            $table->string('name')->nullable()->index();
            $table->decimal('latitude', 10, 8)->default(0)->index();
            $table->decimal('longitude', 11, 8)->default(0)->index();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('area_districts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('regency_id')->nullable()->index();
            $table->string('regency_code')->nullable()->index();
            $table->string('code')->nullable()->index();
            $table->string('name')->nullable()->index();
            $table->decimal('latitude', 10, 8)->default(0)->index();
            $table->decimal('longitude', 11, 8)->default(0)->index();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('area_villages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('district_id')->nullable()->index();
            $table->string('district_code')->nullable()->index();
            $table->string('code')->nullable()->index();
            $table->string('name')->nullable()->index();
            $table->decimal('latitude', 10, 8)->default(0)->index();
            $table->decimal('longitude', 11, 8)->default(0)->index();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_countries');
        Schema::dropIfExists('area_provinces');
        Schema::dropIfExists('area_regencies');
        Schema::dropIfExists('area_districts');
        Schema::dropIfExists('area_villages');
    }
}
