<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtherTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Notification
        Schema::create('auth_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable()->index();
            $table->string('subject')->nullable()->index();
            $table->text('sort_content')->nullable();
            $table->longText('content')->nullable();
            $table->dateTime('readed_at')->nullable()->index();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        // Attachment
        Schema::create('app_attachments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('uuid', 36);
            $table->unsignedBigInteger('model_id')->nullable()->index();
            $table->string('model_type')->nullable()->index();
            $table->string('file_name')->nullable()->index();
            $table->string('file_type')->nullable()->index();
            $table->Integer('file_size')->default(0)->index();
            $table->longText('file_path')->nullable();
            $table->longText('file_url')->nullable();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('ref_paths', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('model_id')->index();
            $table->string('model_type')->index();
            $table->text('path');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_notifications');
        Schema::dropIfExists('app_attachments');
        Schema::dropIfExists('ref_paths');
    }
}
