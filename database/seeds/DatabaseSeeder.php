<?php

use Illuminate\Database\Seeder;
use App\Models\Seeds\AuthSeed;
use App\Models\Seeds\ReferenceSeed;
use App\Models\Seeds\ConfigSeed;

class DatabaseSeeder extends Seeder
{

    const DEFAULT_ADMIN_USERNAME = "admin";
    const DEFAULT_ADMIN_EMAIL = "admin@laravel.com";
    const DEFAULT_ADMIN_PASSWORD = "secret";
    
    public function run(){

        $this->command->warn('Memulai instalasi, silahkan tunggu beberapa saat.');
        
        AuthSeed::init();
        ReferenceSeed::init();
        ConfigSeed::init();

        $this->command->info('-----------------------------------------');
        $this->command->info('Admin Username : '.self::DEFAULT_ADMIN_USERNAME);
        $this->command->info('Admin Email : '.self::DEFAULT_ADMIN_EMAIL);
        $this->command->info('Admin Password : secret');
        $this->command->info('-----------------------------------------');
        $this->command->call('auth:route-permission');
        $this->command->warn('Instalasi Telah Selesai :)');
    }

}
