@extends('layouts.app')
@section('title') Summary @endsection
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active">Summary</li>
                    </ol>
                </div>
                <h4 class="page-title">Summary</h4>
            </div>
        </div>
    </div>


</div>
@endsection