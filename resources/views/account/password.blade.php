@extends('layouts.app')
@section('title') Ubah Password @endsection
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Akun Pengguna</a></li>
                        <li class="breadcrumb-item active">Ubah Password</li>
                    </ol>
                </div>
                <h4 class="page-title">Ubah Password</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            @include('layouts.alert')
            <div class="card">
                
                <div class="card-body">
 
                    <h4 class="header-title">
                        <i class="fas fa-edit"></i>&nbsp;Form Ubah Password
                    </h4>
                    <p class="sub-header">
                       Silahkan lengkapi isian form dibawah ini.
                    </p>

                    <form class="form-horizontal" id="form-submit" method="POST" action="{{ route('password.update') }}" autocomplete="off">
                        {{ csrf_field() }}

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Kata Sandi Lama <span class="text-danger">*</span>
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="password" id="current-password" name="old_password" required="required" class="form-control {{ $errors->has('old_password') ? ' is-invalid' : '' }}">
                                @if($errors->has('old_password'))
                                    <div class="invalid-feedback">{{$errors->first('old_password')}}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Kata Sandi Baru <span class="text-danger">*</span>
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="password" id="new-password" name="password" required="required"  class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}">
                                @if($errors->has('password'))
                                    <div class="invalid-feedback">{{$errors->first('password')}}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Konfirmasi Kata Sandi Baru <span class="text-danger">*</span>
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="password" id="new-password-confirmation" name="password_confirmation" required="required" class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}">
                                @if($errors->has('password_confirmation'))
                                    <div class="invalid-feedback">{{$errors->first('password_confirmation')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label"></label>
                            <div class="col-8 col-xl-10">
                                <button type="reset" data-toggle="tooltip" title="Reset Form" class="btn btn-info"><i class="fas fa-sync"></i>&nbsp;Reset Form</button>
                                <button type="submit" data-toggle="tooltip" title="Simpan Perubahan" class="btn btn-success"><i class="fas fa-save"></i>&nbsp;Simpan Perubahan</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>



</div>
@endsection