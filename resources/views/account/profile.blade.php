@extends('layouts.app')
@section('title') Profil Saya @endsection
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Akun Pengguna</a></li>
                        <li class="breadcrumb-item active">Profil Saya</li>
                    </ol>
                </div>
                <h4 class="page-title">Profil Saya</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-3">
            @include('account.profile-image')
        </div>
        <div class="col-9">
            @include('account.profile-data')
        </div>
    </div>



</div>
@endsection