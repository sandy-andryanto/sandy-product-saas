@extends('layouts.app')
@section('title') Pemberitahuan @endsection
@section('script')
    <script src="{{ asset($script) }}"></script>
@endsection
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Akun Pengguna</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('notifications.index') }}">Pemberitahuan</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
                <h4 class="page-title">Pemberitahuan</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            @include('layouts.alert')
            <div class="card">
                
                <div class="card-body">
 
                    <div class="clearfix">
                        <div class="float-start">
                            <h4 class="header-title">
                                <i class="fas fa-search"></i>&nbsp;Detail {{ $title }}
                            </h4>        
                        </div>
                        <div class="float-end">
                            <a href="{{ route($route.".index") }}" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Kembali ke daftar">
                                <i class="fas fa-arrow-left"></i>&nbsp;Kembali
                            </a>
                            @can('delete_'.$route)
                                <a href="{{ route($route.".destroy", array('id'=> $model->id)) }}" id="btn-delete" data-redirect="{{ route($route.".index") }}" data-id="{{ $model->id }}" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete Data">
                                    <i class="fas fa-trash"></i>&nbsp;Hapus
                                </a>
                            @endcan
                        </div>
                    </div>
                    <p></p>
                    
                    <div class="table-responsive">
                        <table class="table table-striped mb-0">
                            <tr>
                                <td width="180">Subjek</td>
                                <td>:</td>
                                <td>{{ $model->subject }}</td>
                            </tr>
                            <tr>
                                <td>Konten Singkat</td>
                                <td>:</td>
                                <td>{{ $model->sort_content }}</td>
                            </tr>
                            <tr>
                                <td>Isi Konten Utama</td>
                                <td>:</td>
                                <td>{{ $model->content }}</td>
                            </tr>
                            <tr>
                                <td>Dibaca Pada</td>
                                <td>:</td>
                                <td>{{ $model->readed_at }}</td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>



</div>
@endsection