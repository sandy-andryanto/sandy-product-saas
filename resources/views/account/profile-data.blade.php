<div class="card">
    
    <div class="card-body">

        <h4 class="header-title">
            <i class="fas fa-edit"></i>&nbsp;Form Profil Saya
        </h4>
        <p class="sub-header">
            Silahkan lengkapi isian form dibawah ini.
        </p>

        <form class="form-horizontal" id="form-submit" method="POST" action="{{ route('profile.update') }}" autocomplete="off">
            {{ csrf_field() }}

            @include('layouts.alert')

            <div class="row mb-3">
                <label class="col-4 col-xl-2 col-form-label">
                    Username <span class="text-danger">*</span>
                </label>
                <div class="col-8 col-xl-10">
                    <input type="text" id="username" name="username" required="required" value="{{ $user->username }}" class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}">
                    @if($errors->has('username'))
                        <div class="invalid-feedback">{{$errors->first('username')}}</div>
                    @endif
                </div>
            </div>

            <div class="row mb-3">
                <label class="col-4 col-xl-2 col-form-label">
                    Alamat Email <span class="text-danger">*</span>
                </label>
                <div class="col-8 col-xl-10">
                    <input type="email" id="email" name="email" required="required" value="{{ $user->email }}" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}">
                    @if($errors->has('email'))
                        <div class="invalid-feedback">{{$errors->first('email')}}</div>
                    @endif
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-4 col-xl-2 col-form-label">
                    Nomor Telepon
                </label>
                <div class="col-8 col-xl-10">
                    <input type="text" id="phone" name="phone" value="{{ $user->phone }}" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}">
                    @if($errors->has('phone'))
                        <div class="invalid-feedback">{{$errors->first('phone')}}</div>
                    @endif
                </div>
            </div>

            <div class="row mb-3">
                <label class="col-4 col-xl-2 col-form-label">
                    Nama Lengkap 
                </label>
                <div class="col-8 col-xl-10">
                    <input type="text" id="full_name" name="full_name"  value="{{ $user->full_name }}" class="form-control">
                </div>
            </div>

            <div class="row mb-3">
                <label class="col-4 col-xl-2 col-form-label">
                    Jenis Kelamin
                </label>
                <div class="col-8 col-xl-10">
                    <select class="select2 form-control" name="gender" id="gender" data-placeholder="Pilih Jenis Kelamin">
                        <option selected disabled></option>
                        <option {{ (int) $user->gender == 1 ? 'selected' : '' }} value="1">Pria</option>
                        <option {{ (int) $user->gender == 2 ? 'selected' : '' }} value="2">Wanita</option>
                    </select>
                </div>
            </div>

            <div class="row mb-3">
                <label class="col-4 col-xl-2 col-form-label">
                    Nama Panggilan 
                </label>
                <div class="col-8 col-xl-10">
                    <input type="text" id="nick_name" name="nick_name"  value="{{ $user->nick_name }}" class="form-control">
                </div>
            </div>

            <div class="row mb-3">
                <label class="col-4 col-xl-2 col-form-label">
                    Kota
                </label>
                <div class="col-8 col-xl-10">
                    <input type="text" id="regency" name="regency"  value="{{ $user->regency }}" class="form-control">
                </div>
            </div>

            <div class="row mb-3">
                <label class="col-4 col-xl-2 col-form-label">
                    Alamat Lengkap
                </label>
                <div class="col-8 col-xl-10">
                    <textarea class="form-control" rows="4" name="address" id="address">{{ $user->address }}</textarea>
                </div>
            </div>

            <div class="row mb-3">
                <label class="col-4 col-xl-2 col-form-label"></label>
                <div class="col-8 col-xl-10">
                    <button type="reset" data-toggle="tooltip" title="Reset Form" class="btn btn-info"><i class="fas fa-sync"></i>&nbsp;Reset Form</button>
                    <button type="submit" data-toggle="tooltip" title="Simpan Perubahan" class="btn btn-success"><i class="fas fa-save"></i>&nbsp;Simpan Perubahan</button>
                </div>
            </div>

        </form>

    </div>
</div>