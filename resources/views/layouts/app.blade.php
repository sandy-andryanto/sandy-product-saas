
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>{{ ConfigHelper::getValueByKey("website-name") }} | @yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="api-token" content={!! json_encode(Auth::guard('api')->tokenById(Auth::User()->id)) !!}>
        <meta name="base-url" content="{{ url('') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @if(\UserHelper::isDemo())
        <meta name="is_demo" content="1">
        @else 
        <meta name="is_demo" content="0">
        @endif

        @include('layouts.stylesheet')
        @yield('stylesheet')
    </head>

    <body>

        <!-- Pre-loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">Memuat Data...</div>
            </div>
        </div>
        <!-- End Preloader-->

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Topbar Start -->
            <div class="navbar-custom">
                @include('layouts.topbar')
            </div>
            <!-- end Topbar -->

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                @include('layouts.left-side-menu')
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                @yield('content')

                <!-- Footer Start -->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                {{ date('Y') }} &copy; {{ ConfigHelper::getValueByKey("website-name") }} - <a href="javascript:void(0);">{{ env('APP_AUTHOR', 'Laravel') }}</a> 
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- end Footer -->

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <!-- Right Sidebar -->
        <div class="right-bar">
            @include('layouts.right-bar')
        </div>
        <!-- /Right-bar -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>
        @include('layouts.script')
        <script src="{{ asset('app/js/app.core.js?'.time()) }}"></script>
        @yield('script')
        @if(\UserHelper::isDemo())
        <script src="{{ asset('app/js/app.demo.js?'.time()) }}"></script>
        @endif
    </body>
</html>