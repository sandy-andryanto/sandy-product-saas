@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="alert" aria-label="Close"></button>
    <i class="fa fa-check"></i>&nbsp;<strong>Berhasil!</strong> {!! $message !!}
</div>
@endif

@if ($message = Session::get('danger'))
<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="alert" aria-label="Close"></button>
    <i class="fa fa-ban"></i>&nbsp;<strong>Pemberitahuan!</strong> {!! $message !!}
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="alert" aria-label="Close"></button>
    <i class="fa fa-exclamation-triangle"></i>&nbsp;<strong>Pemberitahuan!</strong> {!! $message !!}
</div>
@endif

@if ($message = Session::get('info'))
<div class="alert alert-info alert-dismissible bg-info text-white border-0 fade show" role="alert">
    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="alert" aria-label="Close"></button>
    <i class="fa fa-info"></i>&nbsp;<strong>Informasi!</strong> {!! $message !!}
</div>
@endif
