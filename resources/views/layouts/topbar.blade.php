<div class="container-fluid">
    <ul class="list-unstyled topnav-menu float-end mb-0">

        <li class="dropdown d-none d-lg-inline-block">
            <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-toggle="fullscreen" href="#">
                <i class="fe-maximize noti-icon"></i>
            </a>
        </li>

        
        
        <li class="dropdown notification-list topbar-dropdown d-none" id="notif-section">
            <a class="nav-link dropdown-toggle waves-effect waves-light" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <i class="fe-bell noti-icon"></i>
                <span class="badge bg-danger rounded-circle noti-icon-badge total-notif">0</span>
            </a>
            <div class="dropdown-menu dropdown-menu-end dropdown-lg">
                <div class="noti-scroll" data-simplebar id="notit-section-list"></div>
                <!-- All-->
                <a href="{{ route('notifications.index') }}" class="dropdown-item text-center text-primary notify-item notify-all">
                    Lihat Semua Pemberitahuan
                    <i class="fe-arrow-right"></i>
                </a>
            </div>
        </li>

        <li class="dropdown notification-list topbar-dropdown">
            <a class="nav-link dropdown-toggle nav-user me-0 waves-effect waves-light" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <img src="{{ UserHelper::getRealPhoto() }}" alt="user-image" class="rounded-circle img-user">
                <span class="pro-user-name ms-1">
                    {{ UserHelper::getRealName() }} <i class="mdi mdi-chevron-down"></i> 
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-end profile-dropdown ">
                <!-- item-->
                <div class="dropdown-header noti-title">
                    <h6 class="text-overflow m-0">Selamat Datang !</h6>
                </div>

                <!-- item-->
                <a href="{{ route('profile.index') }}" class="dropdown-item notify-item">
                    <i class="fe-user"></i>
                    <span>Profil Saya</span>
                </a>

                <!-- item-->
                <a href="{{ route('password.index') }}" class="dropdown-item notify-item">
                    <i class="fe-lock"></i>
                    <span>Ubah Password</span>
                </a>

                <!-- item-->
                <a href="{{ route('notifications.index') }}" class="dropdown-item notify-item">
                    <i class="fe-bell"></i>
                    <span>Pembeitahuan</span>
                </a>

                <div class="dropdown-divider"></div>

                <!-- item-->
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item notify-item">
                    <i class="fe-log-out"></i>
                    <span>Logout</span>
                </a>

            </div>
        </li>

    </ul>

    <!-- LOGO -->
    <div class="logo-box">
        <a href="index.html" class="logo logo-dark text-center">
            <span class="logo-sm">
                <img src="{{ asset('assets/images/logo-sm.png') }}" alt="" height="22">
                <!-- <span class="logo-lg-text-light">UBold</span> -->
            </span>
            <span class="logo-lg">
                <img src="{{ asset('assets/images/logo-dark.png') }}" alt="" height="20">
                <!-- <span class="logo-lg-text-light">U</span> -->
            </span>
        </a>

        <a href="index.html" class="logo logo-light text-center">
            <span class="logo-sm">
                <img src="{{ asset('assets/images/logo-sm.png') }}" alt="" height="22">
            </span>
            <span class="logo-lg">
                <img src="{{ asset('assets/images/logo-light.png') }}" alt="" height="20">
            </span>
        </a>
    </div>

    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
        <li>
            <button class="button-menu-mobile waves-effect waves-light">
                <i class="fe-menu"></i>
            </button>
        </li>

        <li>
            <!-- Mobile menu toggle (Horizontal Layout)-->
            <a class="navbar-toggle nav-link" data-bs-toggle="collapse" data-bs-target="#topnav-menu-content">
                <div class="lines">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </a>
            <!-- End mobile menu toggle-->
        </li>   

        @if(\UserHelper::isDemo())
        <li class="dropdown d-none d-xl-block">
            <a class="nav-link dropdown-toggle waves-effect waves-light text-warning" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
               {{ \UserHelper::DemoRemainingText() }}
                <i class="mdi mdi-chevron-down"></i> 
            </a>
            <div class="dropdown-menu">
                <!-- item-->
                <a href="https://api.whatsapp.com/send?phone={{ env('APP_WA') }}" target="_blank" class="dropdown-item">
                    <i class="fab fa-whatsapp mr-2"></i>
                    <span>Kontak Whatsapp</span>
                </a>
                <a href="https://t.me/{{ env('APP_TLG') }}" target="_blank" class="dropdown-item">
                    <i class="fab fa-telegram mr-2"></i>
                    <span>Kontak Telegram</span>
                </a>
            </div>
        </li>
        @endif
        
     
    </ul>
    <div class="clearfix"></div>
</div>