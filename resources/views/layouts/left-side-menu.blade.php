<div class="h-100" data-simplebar>

    <!-- User box -->
    <div class="user-box text-center">
        <img src="{{ UserHelper::getRealPhoto() }}" alt="user-img" title="Mat Helme"
            class="rounded-circle avatar-md img-user">
        <div class="dropdown">
            <a href="javascript: void(0);" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block"
                data-bs-toggle="dropdown">{{ UserHelper::getRealName() }}</a>
            <div class="dropdown-menu user-pro-dropdown">

                <!-- item-->
                <a href="{{ route('profile.index') }}" class="dropdown-item notify-item">
                    <i class="fe-user me-1"></i>
                    <span>Profil Saya</span>
                </a>

                <!-- item-->
                <a href="{{ route('password.index') }}" class="dropdown-item notify-item">
                    <i class="fe-lock me-1"></i>
                    <span>Ubah Password</span>
                </a>

                <!-- item-->
                <a href="{{ route('notifications.index') }}" class="dropdown-item notify-item">
                    <i class="fe-lock me-1"></i>
                    <span>Pemberitahuan</span>
                </a>

                <!-- item-->
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item notify-item">
                    <i class="fe-log-out me-1"></i>
                    <span>Logout</span>
                </a>

            </div>
        </div>
        <p class="text-muted">Admin Head</p>
    </div>

    <!--- Sidemenu -->
    <div id="sidebar-menu">

        <ul id="side-menu">
            <li class="menu-title mt-2">Menu Utama</li>
            {!! UserHelper::getAdminMenu() !!}
            <li class="menu-title mt-2">Menu Pengguna</li>
            <li>
                <a href="{{ route('notifications.index') }}">
                    <i data-feather="bell"></i>
                    <span class="badge bg-warning rounded-pill float-end total-notif">0</span>
                    <span> Pemberitahuan </span>
                </a>
            </li>
            <li>
                <a href="{{ route('profile.index') }}">
                    <i data-feather="user-plus"></i>
                    <span> Profil Saya </span>
                </a>
            </li>
            <li>
                <a href="{{ route('password.index') }}">
                    <i data-feather="lock"></i>
                    <span> Password </span>
                </a>
            </li>
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i data-feather="log-out"></i>
                    <span> Keluar </span>
                </a>
            </li>
        </ul>

    </div>
    <!-- End Sidebar -->

    <div class="clearfix"></div>

</div>