@extends('layouts.auth')
@section('title') Login @endsection
@section('content')

<div class="col-md-8 col-lg-6 col-xl-4">
    <div class="card bg-pattern">

        <div class="card-body p-4">
           
            <div class="text-center w-75 m-auto">
                <div class="auth-logo">
                    <a href="index.html" class="logo logo-dark text-center">
                        <span class="logo-lg">
                            <img src="{{ asset('assets/images/logo-dark.png') }}" alt="" height="22">
                        </span>
                    </a>

                    <a href="index.html" class="logo logo-light text-center">
                        <span class="logo-lg">
                            <img src="{{ asset('assets/images/logo-light.png') }}" alt="" height="22">
                        </span>
                    </a>
                </div>
                <p class="text-muted mb-4 mt-3">
                    Silahkan isi form isian dibawah ini dengan benar.
                </p>
            </div>

            <form action="{{ route('register') }}" method="POST" autocomplete="off">
                {{ csrf_field() }}

                @include('layouts.alert')

                <div class="mb-3">
                    <label class="form-label">Username<span class="text-danger">*</span></label>
                    <input class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}" type="text" id="username" name="username"  required="required" value="{{ old('username') }}" placeholder="Silahkan isi username">
                    @if($errors->has('email'))
                        <div class="invalid-feedback">{{$errors->first('email')}}</div>
                    @endif
                </div>

                <div class="mb-3">
                    <label class="form-label">Alamat Email <span class="text-danger">*</span></label>
                    <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" id="email" name="email"  required="required" value="{{ old('email') }}" placeholder="Silahkan isi alamat email">
                    @if($errors->has('email'))
                        <div class="invalid-feedback">{{$errors->first('email')}}</div>
                    @endif
                </div>

                <div class="mb-3">
                    <label for="password" class="form-label">Kata Sandi <span class="text-danger">*</span></label>
                    <div class="input-group input-group-merge">
                        <input type="password" id="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"  required="required" placeholder="Silahkan isi kata sandi">
                        <div class="input-group-text" data-password="false">
                            <span class="password-eye"></span>
                        </div>
                        @if($errors->has('password'))
                            <div class="invalid-feedback">{{$errors->first('password')}}</div>
                        @endif
                    </div>
                </div>

                <div class="mb-3">
                    <label for="password" class="form-label">Konfirmasi Kata Sandi <span class="text-danger">*</span></label>
                    <div class="input-group input-group-merge">
                        <input type="password" id="password_confirmation" name="password_confirmation" class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"  required="required" placeholder="Silahkan isi konfirmasi kata sandi">
                        <div class="input-group-text" data-password="false">
                            <span class="password-eye"></span>
                        </div>
                        @if($errors->has('password_confirmation'))
                            <div class="invalid-feedback">{{$errors->first('password_confirmation')}}</div>
                        @endif
                    </div>
                </div>

                <div class="mb-3">
                    <label class="form-label">Kode Captcha <span class="text-danger">*</span></label>
                    <input type="text" class="form-control {{ $errors->has('captcha') ? ' is-invalid' : '' }}" id="captcha" name="captcha" required="required" placeholder="Silahkan isi kode captcha dibawah ini" value="">
                    @if($errors->has('captcha'))
                        <div class="invalid-feedback">{{$errors->first('captcha')}}</div>
                    @endif
                </div>

                <div id="captcha-section" class="text-center">
                    <span id="captcha-img">
                        {!! captcha_img('flat') !!}
                    </span>
                    <a href="javascript:void(0);" id="btn-reload-captcha" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Ganti kode captcha">
                        <i class="fas fa-undo-alt"></i>
                    </a>
                </div>
                <div class="clearfix"></div>
                <p></p>
                <p></p>
                <p></p>

                
                <div class="text-center d-grid">
                    <button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Buat akun sekarang" type="submit"> 
                        <i class="fas fa-edit"></i>&nbsp;Daftar Akun Sekarang
                    </button>
                </div>

            </form>

            <div class="text-center">
                <h5 class="mt-3 text-muted">Bantuan</h5>
                <ul class="social-list list-inline mt-3 mb-0">
                    <li class="list-inline-item">
                        <a href="https://api.whatsapp.com/send?phone={{ env('APP_WA') }}" target="_blank" class="social-list-item border-success text-success"><i class="mdi mdi-whatsapp"></i></a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://t.me/{{ env('APP_TLG') }}"  target="_blank" class="social-list-item border-info text-info"><i class="mdi mdi-telegram"></i></a>
                    </li>
                </ul>
            </div>

        </div> <!-- end card-body -->
    </div>
    <!-- end card -->

    <div class="row mt-3">
        <div class="col-12 text-center">
            <p class="text-white-50">
                Sudah memiliki akun ? <a data-toggle="tooltip" data-placement="top" title="Klik disini untuk masuk aplikasi" href="{{ route('login') }}" class="text-white ms-1"><b>Masuk disini!</b></a>
            </p>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

</div> <!-- end col -->

@endsection