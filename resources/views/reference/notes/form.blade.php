@extends('layouts.app')
@section('title') {{ $title }} @endsection
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Referensi</a></li>
                        <li class="breadcrumb-item"><a href="{{ route($route.".index") }}">{{ $title }}</a></li>
                        <li class="breadcrumb-item active">{{ is_null($model->id) ? "Tambah" : "Edit" }}</li>
                    </ol>
                </div>
                <h4 class="page-title">{{ $title }}</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            @include('layouts.alert')
            <div class="card">
                
                <div class="card-body">
 
                    <div class="clearfix">
                        <div class="float-start">
                            <h4 class="header-title">
                                <i class="fas fa-edit"></i>&nbsp;Form {{ $title }}
                            </h4>
                            <p class="sub-header">
                               Silahkan lengkapi isian form dibawah ini.
                            </p>
                        </div>
                        <div class="float-end">
                            <a href="{{ route($route.".index") }}" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Kembali ke daftar">
                                <i class="fas fa-arrow-left"></i>&nbsp;Kembali
                            </a>
                        </div>
                    </div>

                    <form class="form-horizontal" id="form-submit" method="POST" action="{{ is_null($model->id) ? route($route.".store") : route($route.".update", array("id"=> $model->id)) }}" autocomplete="off">
                        {{ csrf_field() }}
                        {{ !is_null($model->id) ? method_field('PATCH') : null  }}

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Judul Catatan <span class="text-danger">*</span>
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text" id="title" name="title" required="required" value="{{ $model->title ? $model->title : old('title') }}" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}">
                                @if($errors->has('title'))
                                    <div class="invalid-feedback">{{$errors->first('title')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Isi Catatan
                            </label>
                            <div class="col-8 col-xl-10">
                                <textarea class="form-control" name="body" id="body" rows="8">{{ $model->body ? $model->body : old('body') }}</textarea>
                                @if($errors->has('body'))
                                    <div class="invalid-feedback">{{$errors->first('body')}}</div>
                                @endif
                            </div>
                        </div>
                        

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label"></label>
                            <div class="col-8 col-xl-10">
                                <button type="reset" data-toggle="tooltip" title="Reset Form" class="btn btn-info"><i class="fas fa-sync"></i>&nbsp;Reset Form</button>
                                <button type="submit" data-toggle="tooltip" title="Simpan Perubahan" class="btn btn-success"><i class="fas fa-save"></i>&nbsp;Simpan Perubahan</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>



</div>
@endsection