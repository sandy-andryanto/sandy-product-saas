@extends('layouts.app')
@section('title') {{ $title }} @endsection
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Referensi</a></li>
                        <li class="breadcrumb-item"><a href="{{ route($route.".index") }}">{{ $title }}</a></li>
                        <li class="breadcrumb-item active">{{ is_null($model->id) ? "Tambah" : "Edit" }}</li>
                    </ol>
                </div>
                <h4 class="page-title">{{ $title }}</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            @include('layouts.alert')
            <div class="card">
                
                <div class="card-body">
 
                    <div class="clearfix">
                        <div class="float-start">
                            <h4 class="header-title">
                                <i class="fas fa-edit"></i>&nbsp;Form {{ $title }}
                            </h4>
                            <p class="sub-header">
                               Silahkan lengkapi isian form dibawah ini.
                            </p>
                        </div>
                        <div class="float-end">
                            <a href="{{ route($route.".index") }}" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Kembali ke daftar">
                                <i class="fas fa-arrow-left"></i>&nbsp;Kembali
                            </a>
                        </div>
                    </div>

                    <form class="form-horizontal" id="form-submit" method="POST" action="{{ is_null($model->id) ? route($route.".store") : route($route.".update", array("id"=> $model->id)) }}" autocomplete="off">
                        {{ csrf_field() }}
                        {{ !is_null($model->id) ? method_field('PATCH') : null  }}

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Nama <span class="text-danger">*</span>
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text" id="name" name="name" required="required" value="{{ $model->name ? $model->name : old('name') }}" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}">
                                @if($errors->has('name'))
                                    <div class="invalid-feedback">{{$errors->first('name')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Email <span class="text-danger">*</span>
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="email" id="email" name="email" required="required" value="{{ $model->email ? $model->email : old('email') }}" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}">
                                @if($errors->has('email'))
                                    <div class="invalid-feedback">{{$errors->first('email')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Telepon <span class="text-danger">*</span>
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text" id="phone" name="phone" required="required" value="{{ $model->phone ? $model->phone : old('phone') }}" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}">
                                @if($errors->has('phone'))
                                    <div class="invalid-feedback">{{$errors->first('phone')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Website 
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text" id="website" name="website" value="{{ $model->website ? $model->website : old('website') }}" class="form-control {{ $errors->has('website') ? ' is-invalid' : '' }}">
                                @if($errors->has('website'))
                                    <div class="invalid-feedback">{{$errors->first('website')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Alamat Lengkap 
                            </label>
                            <div class="col-8 col-xl-10">
                                <textarea class="form-control" name="address" id="address" rows="8">{{ $model->address ? $model->address : old('address') }}</textarea>
                                @if($errors->has('address'))
                                    <div class="invalid-feedback">{{$errors->first('address')}}</div>
                                @endif
                            </div>
                        </div>
                        

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label"></label>
                            <div class="col-8 col-xl-10">
                                <button type="reset" data-toggle="tooltip" title="Reset Form" class="btn btn-info"><i class="fas fa-sync"></i>&nbsp;Reset Form</button>
                                <button type="submit" data-toggle="tooltip" title="Simpan Perubahan" class="btn btn-success"><i class="fas fa-save"></i>&nbsp;Simpan Perubahan</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>



</div>
@endsection