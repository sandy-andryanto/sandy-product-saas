@extends('layouts.app')
@section('title') {{ $title }} @endsection
@section('script')
    <script src="{{ asset($script) }}"></script>
@endsection
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Pengaturan</a></li>
                        <li class="breadcrumb-item"><a href="{{ route($route.".index") }}">{{ $title }}</a></li>
                        <li class="breadcrumb-item active">{{ is_null($model->id) ? "Tambah" : "Edit" }}</li>
                    </ol>
                </div>
                <h4 class="page-title">{{ $title }}</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            @include('layouts.alert')
            <div class="card">
                
                <div class="card-body">
 
                    <div class="clearfix">
                        <div class="float-start">
                            <h4 class="header-title">
                                <i class="fas fa-edit"></i>&nbsp;Form {{ $title }}
                            </h4>
                            <p class="sub-header">
                               Silahkan lengkapi isian form dibawah ini.
                            </p>
                        </div>
                        <div class="float-end">
                            <a href="{{ route($route.".index") }}" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Kembali ke daftar">
                                <i class="fas fa-arrow-left"></i>&nbsp;Kembali
                            </a>
                        </div>
                    </div>

                    <form class="form-horizontal" id="form-submit" method="POST" action="{{ is_null($model->id) ? route($route.".store") : route($route.".update", array("id"=> $model->id)) }}" autocomplete="off">
                        {{ csrf_field() }}
                        {{ !is_null($model->id) ? method_field('PATCH') : null  }}

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Nama <span class="text-danger">*</span>
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text" id="name" name="name" required="required" value="{{ $model->name ? $model->name : old('name') }}" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}">
                                @if($errors->has('name'))
                                    <div class="invalid-feedback">{{$errors->first('name')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Deskripsi
                            </label>
                            <div class="col-8 col-xl-10">
                                <textarea class="form-control" name="description" id="description" rows="4">{{ $model->description ? $model->description : old('description') }}</textarea>
                                @if($errors->has('description'))
                                    <div class="invalid-feedback">{{$errors->first('description')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Detail Hak Akses
                            </label>
                            <div class="col-8 col-xl-10">

                                @if (!is_null($model->id))
                                <input type="hidden" class="is_edit" value="1" />
                                    <input type="hidden" id="permission_data" value='{!! json_encode($permission_data) !!}' />
                                    <input type="hidden" id="route_id" value='{{ $model->id }}' />
                                @endif

                                <table class="table table-bordered" id="table-create-edit">
                                    <tr class="text-center bg-dark text-light">
                                        <td rowspan="3" class="text-center"><strong>Modul & Fitur Aplikasi</strong></td>
                                        <td colspan="4" class="text-center"><strong><input type="checkbox"
                                                    id="checked-all" />&nbsp;Pilih Semua</strong></td>
                                    </tr>
                                    <tr class="text-center bg-dark text-light">
                                        <td><strong><i class="fas fa-search"></i>&nbsp;View</strong></td>
                                        <td><strong><i class="fas fa-plus"></i>&nbsp;Tambah</strong></td>
                                        <td><strong><i class="fas fa-edit"></i>&nbsp;Edit</strong></td>
                                        <td><strong><i class="fas fa-trash"></i>&nbsp;Hapus</strong></td>
                                    </tr>
                                    <tr class="text-center bg-dark text-light">
                                        <td><input type="checkbox" class=" view checked-header" id="checked-view"></td>
                                        <td><input type="checkbox" class=" create checked-header" id="checked-create"></td>
                                        <td><input type="checkbox" class=" edit checked-header" id="checked-edit"></td>
                                        <td><input type="checkbox" class=" delete checked-header" id="checked-delete"></td>
                                    </tr>
                                    {!! UserHelper::generateTablePermission() !!}
                                </table>
                               
                                @if($errors->has('description'))
                                    <div class="invalid-feedback">
                                        Silahkan pilih detail hak akses
                                    </div>
                                @endif
                            </div>
                        </div>
                        

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label"></label>
                            <div class="col-8 col-xl-10">
                                <button type="reset" data-toggle="tooltip" title="Reset Form" class="btn btn-info"><i class="fas fa-sync"></i>&nbsp;Reset Form</button>
                                <button type="submit" data-toggle="tooltip" title="Simpan Perubahan" class="btn btn-success"><i class="fas fa-save"></i>&nbsp;Simpan Perubahan</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>



</div>
@endsection