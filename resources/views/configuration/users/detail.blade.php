@extends('layouts.app')
@section('title') {{ $title }} @endsection
@section('script')
    <script src="{{ asset($script) }}"></script>
@endsection
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Pengaturan</a></li>
                        <li class="breadcrumb-item"><a href="{{ route($route.".index") }}">{{ $title }}</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
                <h4 class="page-title">{{ $title }}</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            @include('layouts.alert')
            <div class="card">
                
                <div class="card-body">
 
                    <div class="clearfix">
                        <div class="float-start">
                            <h4 class="header-title">
                                <i class="fas fa-search"></i>&nbsp;Detail {{ $title }}
                            </h4>        
                        </div>
                        <div class="float-end">
                            <a href="{{ route($route.".index") }}" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Kembali ke daftar">
                                <i class="fas fa-arrow-left"></i>&nbsp;Kembali
                            </a>
                            @can('add_'.$route)
                                <a href="{{ route($route.".create") }}" class="btn  btn-success" data-toggle='tooltip' data-placement='top' title='Tambah data baru'>
                                    <i class="fas fa-plus"></i>&nbsp;Tambah
                                </a>
                            @endcan
                            @can('edit_'.$route)
                                <a href="{{ route($route.".edit", array('id'=> $model->id)) }}" class="btn btn-warning" data-toggle='tooltip' data-placement='top' title='Edit Data'>
                                    <i class="fas fa-edit"></i>&nbsp;Edit
                                </a>
                            @endcan
                            @can('delete_'.$route)
                                <a href="{{ route($route.".destroy", array('id'=> $model->id)) }}" id="btn-delete" data-redirect="{{ route($route.".index") }}" data-id="{{ $model->id }}" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete Data">
                                    <i class="fas fa-trash"></i>&nbsp;Hapus
                                </a>
                            @endcan
                        </div>
                    </div>
                    <p></p>

                    
                    <div class="table-responsive">
                        <table class="table table-striped mb-0">
                            <tr>
                                <td width="160">Nama Lengkap</td>
                                <td width="20">:</td>
                                <td>{{ $model->full_name }}</td>
                            </tr>
                            <tr>
                                <td>Nama Panggilan</td>
                                <td>:</td>
                                <td>{{ $model->nick_name }}</td>
                            </tr>
                            <tr>
                                <td>Jenis Kelamin</td>
                                <td>:</td>
                                <td>{{ (int) $model->gender == 1 ? 'Pria' : 'Wanita' }}</td>
                            </tr>
                            <tr>
                                <td>Kota</td>
                                <td>:</td>
                                <td>{{ $model->regency }}</td>
                            </tr>
                            <tr>
                                <td>Alamat Lengkap</td>
                                <td>:</td>
                                <td>{{ $model->address }}</td>
                            </tr>
                            <tr>
                                <td>Username</td>
                                <td>:</td>
                                <td>{{ $model->username }}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>:</td>
                                <td>{{ $model->email }}</td>
                            </tr>
                            <tr>
                                <td>Telepon</td>
                                <td>:</td>
                                <td>{{ $model->phone }}</td>
                            </tr>
                            <tr>
                                <td>Backend</td>
                                <td>:</td>
                                <td>{{ (int) $model->is_admin == 1 ? 'Ya' : 'Tidak' }}</td>
                            </tr>
                            <tr>
                                <td>Blokir</td>
                                <td>:</td>
                                <td>{{ (int) $model->is_banned == 1 ? 'Ya' : 'Tidak' }}</td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>:</td>
                                <td>{{ (int) $model->verified == 1 ? 'Terverifikasi' : 'Belum Diverifikasi' }}
                                </td>
                            </tr>
                            <tr>
                                <td>Hak Akses</td>
                                <td>:</td>
                                <td>{{ implode(', ', $model->Roles->pluck('name')->toArray()) }}</td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>



</div>
@endsection