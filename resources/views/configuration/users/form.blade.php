@extends('layouts.app')
@section('title') {{ $title }} @endsection
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Pengaturan</a></li>
                        <li class="breadcrumb-item"><a href="{{ route($route.".index") }}">{{ $title }}</a></li>
                        <li class="breadcrumb-item active">{{ is_null($model->id) ? "Tambah" : "Edit" }}</li>
                    </ol>
                </div>
                <h4 class="page-title">{{ $title }}</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            @include('layouts.alert')
            <div class="card">
                
                <div class="card-body">
 
                    <div class="clearfix">
                        <div class="float-start">
                            <h4 class="header-title">
                                <i class="fas fa-edit"></i>&nbsp;Form {{ $title }}
                            </h4>
                            <p class="sub-header">
                               Silahkan lengkapi isian form dibawah ini.
                            </p>
                        </div>
                        <div class="float-end">
                            <a href="{{ route($route.".index") }}" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Kembali ke daftar">
                                <i class="fas fa-arrow-left"></i>&nbsp;Kembali
                            </a>
                        </div>
                    </div>

                    <form class="form-horizontal" id="form-submit" method="POST" action="{{ is_null($model->id) ? route($route.".store") : route($route.".update", array("id"=> $model->id)) }}" autocomplete="off">
                        {{ csrf_field() }}
                        {{ !is_null($model->id) ? method_field('PATCH') : null  }}

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Username <span class="text-danger">*</span>
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="username" id="username" name="username" required="required" value="{{ $model->username ? $model->username : old('username') }}" class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}">
                                @if($errors->has('username'))
                                    <div class="invalid-feedback">{{$errors->first('username')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Email <span class="text-danger">*</span>
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="email" id="email" name="email" required="required" value="{{ $model->email ? $model->email : old('email') }}" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}">
                                @if($errors->has('email'))
                                    <div class="invalid-feedback">{{$errors->first('email')}}</div>
                                @endif
                            </div>
                        </div>

                        @if(is_null($model->id))
                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Kata Sandi  <span class="text-danger">*</span>
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="password" id="password" name="password" required="required" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}">
                                @if($errors->has('password'))
                                    <div class="invalid-feedback">{{$errors->first('password')}}</div>
                                @endif
                            </div>
                        </div>
                        @else 
                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Kata Sandi 
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="password" id="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}">
                                @if($errors->has('password'))
                                    <div class="invalid-feedback">{{$errors->first('password')}}</div>
                                @endif
                            </div>
                        </div>
                        @endif

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Hak Akses
                            </label>
                            <div class="col-8 col-xl-10">
                                {!! Form::select('roles', $roles, $role_selected, ['class' => 'form-control select2', 'multiple' => 'multiple', 'name' => 'roles[]']) !!}
                                @if($errors->has('roles'))
                                    <div class="invalid-feedback">{{$errors->first('roles')}}</div>
                                @endif
                            </div>
                        </div>
                        

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Telepon 
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text" id="phone" name="phone" value="{{ $model->phone ? $model->phone : old('phone') }}" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}">
                                @if($errors->has('phone'))
                                    <div class="invalid-feedback">{{$errors->first('phone')}}</div>
                                @endif
                            </div>
                        </div>

                       

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Nama Lengkap
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text" id="full_name" name="full_name"  value="{{ $model->full_name ? $model->full_name : old('full_name') }}" class="form-control {{ $errors->has('full_name') ? ' is-invalid' : '' }}">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Nama Paggilan
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text" id="nick_name" name="nick_name"  value="{{ $model->nick_name ? $model->nick_name : old('nick_name') }}" class="form-control {{ $errors->has('nick_name') ? ' is-invalid' : '' }}">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Jenis Kelamin
                            </label>
                            <div class="col-8 col-xl-10">
                                <select class="select2 form-control" name="gender" id="gender" data-placeholder="Pilih Jenis Kelamin">
                                    <option selected disabled></option>
                                    <option {{ (int) $model->gender == 1 ? 'selected' : '' }} value="1">Pria</option>
                                    <option {{ (int) $model->gender == 2 ? 'selected' : '' }} value="2">Wanita</option>
                                </select>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Kota
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text" id="regency" name="regency"  value="{{ $model->regency ? $model->regency : old('regency') }}" class="form-control {{ $errors->has('regency') ? ' is-invalid' : '' }}">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Alamat Lengkap 
                            </label>
                            <div class="col-8 col-xl-10">
                                <textarea class="form-control" name="address" id="address" rows="8">{{ $model->address ? $model->address : old('address') }}</textarea>
                                @if($errors->has('address'))
                                    <div class="invalid-feedback">{{$errors->first('address')}}</div>
                                @endif
                            </div>
                        </div>
                        

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label"></label>
                            <div class="col-8 col-xl-10">
                                <button type="reset" data-toggle="tooltip" title="Reset Form" class="btn btn-info"><i class="fas fa-sync"></i>&nbsp;Reset Form</button>
                                <button type="submit" data-toggle="tooltip" title="Simpan Perubahan" class="btn btn-success"><i class="fas fa-save"></i>&nbsp;Simpan Perubahan</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>



</div>
@endsection