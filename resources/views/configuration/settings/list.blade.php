@extends('layouts.app')
@section('title') {{ $title }} @endsection
@section('script')
    <script src="{{ asset($script) }}"></script>
@endsection
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Pengaturan</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route($route.".index") }}">{{ $title }}</a></li>
                    </ol>
                </div>
                <h4 class="page-title">{{ $title }}</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-3">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">
                        <i class="fas fa-cogs"></i>&nbsp;Artisan
                    </h4>
                    <p class="sub-header">
                       Silahkan lengkapi generate artisan.
                    </p>
                    <a href="javascript:void(0);" data-value="config:cache" class="btn btn-primary w-100 btn-sm btn-w100 uppercase btn-artisan">
                        config cache
                    </a>
                    <p></p>
                    <a href="javascript:void(0);" data-value="cache:clear" class="btn btn-primary w-100 btn-sm btn-w100  uppercase btn-artisan">
                        cache clear
                    </a>
                    <p></p>
                    <a href="javascript:void(0);" data-value="config:cache" class="btn btn-primary w-100 btn-sm btn-w100  uppercase btn-artisan">
                        config clear
                    </a>
                    <p></p>
                    <a href="javascript:void(0);" data-value="route:scan" class="btn btn-primary w-100 btn-sm btn-w100  uppercase btn-artisan">
                        route scan
                    </a>
                    <p></p>
                    <a href="javascript:void(0);" data-value="key:generate" class="btn btn-primary w-100 btn-sm btn-w100  uppercase btn-artisan">
                        key generate
                    </a>
                    <p></p>
                    <a href="javascript:void(0);" data-value="model:scan" class="btn btn-primary w-100 btn-sm btn-w100  uppercase btn-artisan">
                        model scan
                    </a>
                    <p></p>
                    <a href="javascript:void(0);" data-value="permission:cache-reset" class="btn btn-primary w-100 btn-sm btn-w100  uppercase btn-artisan">
                        permission cache reset
                    </a>
                    <p></p>
                    <a href="javascript:void(0);" data-value="view:clear" class="btn btn-primary w-100 btn-sm btn-w100 uppercase btn-artisan">
                        view clear
                    </a>
                    <p></p>
                    <a href="javascript:void(0);" data-value="auth:route-permission" class="btn btn-primary w-100 btn-sm btn-w100  uppercase btn-artisan">
                        auth route permission
                    </a>
                </div>
            </div>
        </div>
        <div class="col-9">
            @include('layouts.alert')
            <div class="card">
                
                <div class="card-body">
 
                    <div class="clearfix">
                        <div class="float-start">
                            <h4 class="header-title">
                                <i class="fas fa-edit"></i>&nbsp;Form {{ $title }}
                            </h4>
                            <p class="sub-header">
                               Silahkan lengkapi isian form dibawah ini.
                            </p>
                        </div>
                        <div class="float-end">
                            <a href="{{ route($route.".index") }}" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Kembali ke daftar">
                                <i class="fas fa-arrow-left"></i>&nbsp;Kembali
                            </a>
                        </div>
                    </div>

                    <form class="form-horizontal" id="form-submit" method="POST" enctype="multipart/form-data"  action="{{ route($route.'.store') }}" autocomplete="off">
                        {{ csrf_field() }}
                       
                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Nama Website 
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text" id="website-name" name="website-name" class="form-control" value="{{ ConfigHelper::getValueByKey('website-name') }}">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Mata Uang
                            </label>
                            <div class="col-8 col-xl-10">
                                <select class="select2 form-control" name="currency-code" id="currency-code" data-placeholder="--Pilih Mata Uang --">
                                    <option></option>
                                    @foreach($currencies as $row => $key)
                                        @php 
                                            $selected_code = ConfigHelper::getValueByKey('currency-code');
                                            $selected = $selected_code ==  $row ? "selected" : "";
                                        @endphp
                                        <option value="{{ $row }}" {{ $selected }}>{{ $row }} ( {{ $key }} ) </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Fav Icon
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="file" name="favicon" class="form-control" />
                                @if(ConfigHelper::getValueByKey('favicon') != 'assets/app/img/logo.png')
                                    <h1></h1>
                                    <img src="{{ url(ConfigHelper::getValueByKey('favicon') ) }}" class="img-thumbnail img-fluid" width="100" />
                                @endif  
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Zona Waktu
                            </label Waktu>
                            <div class="col-8 col-xl-10">
                                <select class="select2 form-control" name="timezone" id="timezone" data-placeholder="--Pilih Zona Waktu --">
                                    <option></option>
                                    @foreach($timezones as $row => $key)
                                        @php 
                                            $selected_code = ConfigHelper::getValueByKey('timezone');
                                            $selected = $selected_code ==  $row ? "selected" : "";
                                        @endphp
                                        <option value="{{ $row }}" {{ $selected }}>{{ $row }} | {{ $key }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Mail Driver
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text"  name="mail-driver"  value="{{ ConfigHelper::getValueByKey('mail-driver') }}" class="form-control">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Mail Host
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text"  name="mail-host"  value="{{ ConfigHelper::getValueByKey('mail-host') }}" class="form-control">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Mail Port
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text"  name="mail-port"  value="{{ ConfigHelper::getValueByKey('mail-port') }}" class="form-control">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Mail Username
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text"  name="mail-username"  value="{{ ConfigHelper::getValueByKey('mail-username') }}" class="form-control">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Mail Password
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text"  name="mail-password"  value="{{ ConfigHelper::getValueByKey('mail-password') }}" class="form-control">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Mail Encription
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text"  name="mail-encryption"  value="{{ ConfigHelper::getValueByKey('mail-encryption') }}" class="form-control">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Mail Address
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text"  name="mail-address"  value="{{ ConfigHelper::getValueByKey('mail-address') }}" class="form-control">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label">
                                Mail Name
                            </label>
                            <div class="col-8 col-xl-10">
                                <input type="text"  name="mail-address"  value="{{ ConfigHelper::getValueByKey('mail-address') }}" class="form-control">
                            </div>
                        </div>

                        
                        <div class="row mb-3">
                            <label class="col-4 col-xl-2 col-form-label"></label>
                            <div class="col-8 col-xl-10">
                                <button type="reset" data-toggle="tooltip" title="Reset Form" class="btn btn-info"><i class="fas fa-sync"></i>&nbsp;Reset Form</button>
                                <button type="submit" data-toggle="tooltip" title="Simpan Perubahan" class="btn btn-success"><i class="fas fa-save"></i>&nbsp;Simpan Perubahan</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>



</div>
@endsection