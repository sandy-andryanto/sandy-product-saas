@extends('layouts.app')
@section('title')  {{ $title }} @endsection
@section('script')
    <script src="{{ asset($script) }}"></script>
@endsection
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Pengaturan</a></li>
                        <li class="breadcrumb-item active"> {{ $title }}</li>
                    </ol>
                </div>
                <h4 class="page-title"> {{ $title }}</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            @include('layouts.alert')
            <div class="card">
                
                <div class="card-body">
 
                    <h4 class="header-title">
                        <i class="fas fa-table"></i>&nbsp;Daftar {{ $title }}
                    </h4>

                    <div class="table-responsive">
                        <table class="table table-striped mb-0" id="table-data">
                            <thead>
                                <tr class="table-dark">
                                    <th width="20" class="text-center">No</th>
                                    <th>Waktu</th>
                                    <th>Instance</th>
                                    <th>Chanel</th>
                                    <th>Level</th>
                                    <th>Level Name</th>
                                    <th>Message</th>
                                    <th>Context</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>



</div>
@endsection