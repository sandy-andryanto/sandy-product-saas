<?php 

namespace App\Models\Seeds;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

class ReferenceSeed extends Model{

    public static function init(){
        
       
        DB::statement("DELETE FROM ref_contacts WHERE id <> 0");
        DB::statement("DELETE FROM ref_notes WHERE id <> 0");
        
        for($i = 1; $i <=50; $i++){
            $company = DB::table("auth_companies")->inRandomOrder()->first();
            $faker = Faker::create("id_ID");
            $items = array(
                "company_id"=> $company->id,
                'name'=> $faker->name,
                'email'=> $faker->safeEmail,
                'phone'=> $faker->phoneNumber,
                'website'=> $faker->safeEmailDomain,
                "address"=> $faker->streetAddress,
                "created_at"=> now(),
                "updated_at"=> now()
            );
            DB::table("ref_contacts")->insert($items);
        }

        for($i = 1; $i <=50; $i++){
            $company = DB::table("auth_companies")->inRandomOrder()->first();
            $faker = Faker::create("id_ID");
            $items = array(
                "company_id"=> $company->id,
                'title'=> $faker->sentence,
                'body'=> $faker->paragraph(10, true),
                "created_at"=> now(),
                "updated_at"=> now()
            );
            DB::table("ref_notes")->insert($items);
        }

         


    
    }


}