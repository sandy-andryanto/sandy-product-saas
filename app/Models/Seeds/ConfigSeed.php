<?php 

namespace App\Models\Seeds;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

class ConfigSeed extends Model{

    public static function init(){
        $faker = Faker::create("id_ID");
        $config = array(
            "company-logo"=> "app/img/logo.png",
			"currency-code"=> $faker->currencyCode,
			"favicon"=> "app/img/logo.png",
			"footer-invoice"=> "<b>Header Invoice</b>",
			"header-invoice"=>  "<b>Footer Invoice</b>",
			"invoice-message"=> "<b>Thank You For Visit</b>",
			"mail-driver"=> "smtp",
            "mail-host"=> "smtp.mailtrap.io",
            "mail-port"=> "2525",
            "mail-username"=> "c535da0ec172f9",
            "mail-password"=> "4403a18c50be55",
            "mail-encryption"=> "tls",
            "mail-address"=> "from@example.com",
            "mail-name"=> "Example",
			"sales-tax"=> "10",
			"timezone"=> $faker->timezone,
			"website-name"=> $faker->company,
            "frontend"=> "Y",
            "company-name"=> "Seblakware Studio"
        );
        DB::statement("DELETE FROM app_settings WHERE id <> 0");
        foreach($config as $row => $key){
            $items = array(
                "key_name"=> $row,
                "key_value"=> $key,
                "created_at"=> now(),
                "updated_at"=> now()
            );
            DB::table("app_settings")->insert($items);
        }
    }

}