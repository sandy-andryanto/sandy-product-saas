<?php 

namespace App\Models\Seeds;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

class DeleteExpired extends Model{

    public static function init(){
       
        $tables = array(
            "auth_users",
            "ref_contacts",
            "ref_notes"
        );

        $companies = DB::table("auth_companies")->where("date_expired", "<", date("Y-m-d"))->get();
        if ($companies->isNotEmpty()) {
            foreach($companies as $company){
                $company_id = $company->id;
                foreach($tables as $table){
                    DB::statement("DELETE FROM ".$table." WHERE company_id = ".$company_id);
                }
                DB::table("auth_companies")->where("id", $company_id)->delete();
            }
        }
        DB::statement("DELETE FROM auth_model_has_roles WHERE model_id NOT IN (SELECT id FROM auth_users)");

    }

}