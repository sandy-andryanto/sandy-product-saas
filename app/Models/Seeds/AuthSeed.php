<?php 

namespace App\Models\Seeds;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;
// Load Models
use App\Models\Entities\Auth\User;
use App\Models\Entities\Auth\Role;
use App\Models\Entities\Auth\Permission;

class AuthSeed extends Model{

    const DEFAULT_ADMIN_USERNAME = "admin";
    const DEFAULT_ADMIN_EMAIL = "admin@laravel.com";
    const DEFAULT_ADMIN_PASSWORD = "secret";

    public static function init(){

        // Create Company
        DB::statement("DELETE FROM auth_companies WHERE id <> 0");
        for($i = 1; $i <=5; $i++){
            $faker = Faker::create("id_ID");
            $items = array(
                'name'=> $faker->company,
                'email'=> $faker->safeEmail,
                'phone'=> $faker->phoneNumber,
                'website'=> $faker->safeEmailDomain,
                "address"=> $faker->streetAddress,
                "date_expired"=> date("Y-12-31"),
                "created_at"=> now(),
                "updated_at"=> now()
            );
            DB::table("auth_companies")->insert($items);
        }

        // Create Roles
        $defaultRoles = array("ADMIN", "CLIENT");
        foreach($defaultRoles as $role){
            $roleCurrent = Role::where("name", $role)->first();
            if(is_null($roleCurrent)){
                Role::firstOrCreate(['name' => trim($role)]);
            }
        }

        // Create Users
        $roles = Role::all();
        foreach($roles as $role){
            if($role->name == 'ADMIN'){
                self::createUser($role, true);
            }else{
                for($i=1; $i <=20; $i++){
                    self::createUser($role, false);
                }
            }
        }

        // Create Notification
        DB::statement("DELETE FROM auth_notifications WHERE id <> 0");
        $users = User::all();
        foreach($users as $user){
            for($i = 1; $i <= 25; $i++){
                $faker = Faker::create("id_ID");
                $items = array(
                    'user_id'=> $user->id,
                    'subject'=> $faker->paragraph(1, true),
                    'sort_content'=> $faker->paragraph(2, true),
                    'content'=> $faker->paragraph(10, true),
                    "created_at"=> now(),
                    "updated_at"=> now()
                );
                DB::table("auth_notifications")->insert($items);
            }
        }

    }

    public static function createUser($role, $is_admin){
        $faker = Faker::create("id_ID");
        $username = $is_admin ? self::DEFAULT_ADMIN_USERNAME : $faker->userName;
        $email = $is_admin ? self::DEFAULT_ADMIN_EMAIL : $faker->safeEmail;
        $password = bcrypt(self::DEFAULT_ADMIN_PASSWORD);
        $admin = $is_admin ? 1 : 0;
        $gender = rand(1,2);
        $company = DB::table("auth_companies")->inRandomOrder()->first();
        $formData = array(
            "company_id"=> $company->id,
            'full_name'=> $faker->name($gender == 1 ? "male" : "female"),
            'nick_name'=> $gender == 1 ? $faker->firstNameMale : $faker->firstNameFemale,
            'gender'=> $gender,
            "regency"=> $faker->city,
            "address"=> $faker->streetAddress,
            'username'=> $username,
            'email'=> $email,
            'phone'=> $faker->phoneNumber,
            'password'=> $password,
            'session_id'=> base64_encode($username),
            'is_banned'=> 0,
            'is_admin'=> $admin,
            'remember_token'=> base64_encode($username),
            'verified'=> 1,
            'verification_token'=> base64_encode($email)
        );
        $user = User::create($formData);
        $user->assignRole($role->name);
    }
  

}