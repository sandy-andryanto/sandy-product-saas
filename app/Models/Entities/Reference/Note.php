<?php 

namespace App\Models\Entities\Reference;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Note extends Model implements Auditable{

    use \OwenIt\Auditing\Auditable, SoftDeletes;

    protected $table = 'ref_notes';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'title',
        'body',
        'status'
    ];
    
}