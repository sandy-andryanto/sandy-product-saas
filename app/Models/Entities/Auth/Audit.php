<?php 

namespace App\Models\Entities\Auth;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model{

    protected $table = 'auth_audits';
    protected $fillable = [
        'user_id',
        'event',
        'auditable',
        'old_values',
        'new_values',
        'url',
        'ip_address',
        'user_agent',
        'tags'
    ];
    
}