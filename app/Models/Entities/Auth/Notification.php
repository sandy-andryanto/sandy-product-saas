<?php 

namespace App\Models\Entities\Auth;

use Illuminate\Database\Eloquent\Model;
use App\Models\Entities\Auth\User;

class Notification extends Model{

    protected $table = 'auth_notifications';
    protected $fillable = [
        'user_id',
        'subject',
        'sort_content',
        'content',
        'readed_at'
    ];

    public function User(){
        return $this->belongsTo(User::class, "user_id");
    }
    
}