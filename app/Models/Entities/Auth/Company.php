<?php 

namespace App\Models\Entities\Auth;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
// Relations
use App\Models\Entities\Auth\User;

class Company extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'auth_companies';
    protected $fillable = [
        'name',
        'email',
        'phone',
        'website',
        'address',
        'date_expired'
    ];

    public function User() {
        return $this->hasMany(User::class);
    }
    
}