<?php

namespace App\Models\Entities\Auth;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Notifications\ResetPassword;
use Illuminate\Support\Arr;
// Load Models
use App\Models\Entities\Auth\Role;
use App\Models\Entities\Auth\Route;
use App\Models\Entities\Auth\Notification;
use App\Models\Entities\Auth\Company;

class User extends Authenticatable implements Auditable , JWTSubject
{
    use Notifiable, SoftDeletes, HasRoles, \OwenIt\Auditing\Auditable;

    protected $table = "auth_users";

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "company_id",
        "full_name",
        "nick_name",
        "gender",
        "regency",
        "address",
        "username",
        "email",
        "phone",
        "password",
        "session_id",
        "is_banned",
        "is_admin",
        "remember_token",
        "verified",
        "verification_token"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
    ];

    public function getJWTIdentifier(){
        return $this->getKey();
    }

    public function getJWTCustomClaims(){
        return [];
    }

    public function sendPasswordResetNotification($token) {
        $this->notify(new ResetPassword($token));
    }

    public function transformAudit(array $data): array {
        if (Arr::has($data, 'new_values.role_id')) {
            $data['old_values']['role_name'] = Role::find($this->getOriginal('role_id'))->name;
            $data['new_values']['role_name'] = Role::find($this->getAttribute('role_id'))->name;
        }
        return $data;
    }

    public function Notification() {
        return $this->hasMany(Notification::class);
    }

    public function getHomeRoute(){
        $roles = self::Roles()->pluck("id")->toArray();
        return Route::getRouteHome($roles);
    }

    public function Admin(){
        $user = $this;
        $is_admin = (int) $user->is_admin;
        return $is_admin == 1 ? true : false;
    }

    public function Company(){
        return $this->belongsTo(Company::class, "company_id");
    }

}
