<?php 

namespace App\Models\Entities\Auth;

use OwenIt\Auditing\Contracts\Auditable;

class Permission extends \Spatie\Permission\Models\Permission implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'auth_permission';
    protected $fillable = [
        'name',
        'guard_name',
        'dscription'
    ];
}