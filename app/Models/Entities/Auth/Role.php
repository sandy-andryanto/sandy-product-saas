<?php 

namespace App\Models\Entities\Auth;

use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Entities\Auth\Route;
use Illuminate\Support\Facades\DB;

class Role extends \Spatie\Permission\Models\Role implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'auth_roles';
    protected $fillable = [
        'name',
        'guard_name',
        'description'
    ];

    public function Routes() {
        return $this->belongsToMany(Route::class, "auth_routes_roles");
    }

    public static function checkPermission($role_id, $permission_name){
        $permission = DB::table("auth_permissions")->where("name", trim($permission_name))->first();
        if(!is_null($permission)){
            $permission_id = $permission->id;
            $check = DB::table("auth_role_has_permissions")->where("permission_id", $permission_id)->where("role_id", $role_id)->first();
            if(!is_null($check)){
                return true;
            }
        }
        return false;
    }

}