<?php

namespace App\Models\Entities\Utils;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model {

    public static function getData(){
        $json = file_get_contents(storage_path("seeds/json/currencies.json"));
        $currencies = json_decode($json, true);
        return $currencies;
    }

}