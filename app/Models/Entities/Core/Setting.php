<?php 

namespace App\Models\Entities\Core;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Setting extends Model implements Auditable{

    use \OwenIt\Auditing\Auditable;

    protected $table = 'app_settings';
    protected $fillable = [
        'key_name',
        'key_value'
    ];
    
}