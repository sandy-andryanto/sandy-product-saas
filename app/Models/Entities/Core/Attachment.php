<?php 

namespace App\Models\Entities\Core;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Attachment extends Model implements Auditable{

    use \OwenIt\Auditing\Auditable;

    protected $table = 'app_attachments';
    protected $fillable = [
        'uuid',
        'model_id',
        'model_type',
        'file_name',
        'file_type',
        'file_size',
        'file_path',
        'file_url'
    ];

    public static function getRowById($id){
        $result = self::where("uuid", $id)->first();
        return $result;
    }

    public static function getRowByModel($model, $id){
        $result = self::where("model_id", $id)->where("model_type", $model)->first();
        return $result;
    }

    public static function getRowsById($id){
        $result = self::where("uuid", $id)->get();
        return $result;
    }

    public static function getRowsByModel($model, $id){
        $result = self::where("model_id", $id)->where("model_type", $model)->get();
        return $result;
    }
    
}