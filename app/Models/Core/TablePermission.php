<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;
use App\Models\Entities\Auth\Route;

class TablePermission extends Model {

    private $element = null;

    public static function render(){
        $component = new TablePermission;
        return $component->getComponent();
    }

    public function getComponent(){
        $data = Route::where("parent_id", null)->orderBy("sort")->get();    
        if ($data->isNotEmpty()) {
            foreach ($data as $row) {
                $this->tablePermissionRow($row);
            }
        }
        return $this->minify_html($this->element);
    }

    private function tablePermissionRow($parent, $level = 0, $has_child = false){
        $data = Route::where("parent_id", $parent->id)->orderBy("sort")->get(); 
        $menu_id = $parent->id;
        $parentId = $parent->parent_id ? $parent->parent_id : $parent->id;
        $hasChild = $has_child ? "is_child" : "is_parent";
        $isSecure = null;
        $isChecked = null;
        $checkbox = "<input type='checkbox' name='routes[]' value='" . $menu_id . "' id='menu" . $menu_id . "' class=' menu  " . $hasChild . " parent" . $parentId . "' data-menu-id='" . $menu_id . "'  data-parent-id='" . $parentId . "' " . $isChecked . "  />";
        $menuName = $has_child ? "" . $this->checkBoxWrapper($checkbox, $parent->name, $level) : "" . $this->checkBoxWrapper($checkbox, $parent->name, $level) . "";

        if ($data->isNotEmpty()) {
            if (isset($parent->route) && !is_null($parent->route)) {
                $this->element .= "
                    <tr>
                        <td>" . $menuName . "</td>
                        <td class='text-center'>" . $this->checkBoxWrapper("<input type='checkbox' id='can_view" . $menu_id . "' name='can_view" . $menu_id . "' value='1'  class=' permission view'  data-menu-id='" . $menu_id . "'  data-parent-id='" . $parentId . "' " . $isChecked . "  />", null, 0, false) . "</td>
                        <td class='text-center'>" . $this->checkBoxWrapper("<input type='checkbox' id='can_add" . $menu_id . "' name='can_add" . $menu_id . "' value='1'  class=' permission create'  data-menu-id='" . $menu_id . "'  data-parent-id='" . $parentId . "' " . $isChecked . "  />", null, 0, false) . "</td>
                        <td class='text-center'>" . $this->checkBoxWrapper("<input type='checkbox' id='can_edit" . $menu_id . "' name='can_edit" . $menu_id . "' value='1'  class=' permission edit'  data-menu-id='" . $menu_id . "'  data-parent-id='" . $parentId . "' " . $isChecked . "  />", null, 0, false) . "</td>
                        <td class='text-center'>" . $this->checkBoxWrapper("<input type='checkbox' id='can_delete" . $menu_id . "' name='can_delete" . $menu_id . "' value='1'  class=' permission delete'  data-menu-id='" . $menu_id . "'  data-parent-id='" . $parentId . "' " . $isChecked . "  />", null, 0, false) . "</td>
                    </tr>
                ";
            }else{
                $this->element .= "
                    <tr>
                        <td>" . $menuName . "</td>
                        <td class='text-center'><i class='fas fa fa-ban'></i></td>
                        <td class='text-center'><i class='fas fa fa-ban'></i></td>
                        <td class='text-center'><i class='fas fa fa-ban'></i></td>
                        <td class='text-center'><i class='fas fa fa-ban'></i></td>
                    </tr>
                ";
            }
            $level++;
            foreach ($data as $row) {
                $this->tablePermissionRow($row, $level, true);
            }
        }else{
            if (isset($parent->route) && !is_null($parent->route)) {
                $this->element .= "
                    <tr class='" . $isSecure . "'>
                        <td>" . $menuName . "</td>
                        <td class='text-center'>" . $this->checkBoxWrapper("<input type='checkbox' id='can_view" . $menu_id . "' name='can_view" . $menu_id . "' value='1'  class=' permission view'  data-menu-id='" . $menu_id . "'  data-parent-id='" . $parentId . "' " . $isChecked . "  />", null, 0, false) . "</td>
                        <td class='text-center'>" . $this->checkBoxWrapper("<input type='checkbox' id='can_add" . $menu_id . "' name='can_add" . $menu_id . "' value='1'  class=' permission create'  data-menu-id='" . $menu_id . "'  data-parent-id='" . $parentId . "' " . $isChecked . "  />", null, 0, false) . "</td>
                        <td class='text-center'>" . $this->checkBoxWrapper("<input type='checkbox' id='can_edit" . $menu_id . "' name='can_edit" . $menu_id . "' value='1'  class=' permission edit'  data-menu-id='" . $menu_id . "'  data-parent-id='" . $parentId . "' " . $isChecked . "  />", null, 0, false) . "</td>
                        <td class='text-center'>" . $this->checkBoxWrapper("<input type='checkbox' id='can_delete" . $menu_id . "' name='can_delete" . $menu_id . "' value='1'  class=' permission delete'  data-menu-id='" . $menu_id . "'  data-parent-id='" . $parentId . "' " . $isChecked . "  />", null, 0, false) . "</td>
                    </tr>
                ";
            }else{
                $this->element .= "
                    <tr>
                        <td>" . $menuName . "</td>
                        <td class='text-center'><i class='fas fa fa-ban'></i></td>
                        <td class='text-center'><i class='fas fa fa-ban'></i></td>
                        <td class='text-center'><i class='fas fa fa-ban'></i></td>
                        <td class='text-center'><i class='fas fa fa-ban'></i></td>
                    </tr>
                ";
            }
        }

    }

    private function createSpace($param = null) {
        $html = "&nbsp&nbsp";
        $max = $param * 5;
        for ($i = 0; $i < $max; $i++) {
            $html .= "&nbsp;";
        }
        return $html;
    }

    private function checkBoxWrapper($checkbox, $name = null, $space = 0, $withIcon = true) {
        if ($withIcon) {
            return '
                ' . $this->createSpace($space) . '<i class="fas fa-arrow-right"></i>&nbsp;' . $checkbox . '&nbsp;&nbsp;&nbsp;' . $name . '
            ';
        } else {
            return $checkbox;
        }
    }

    private function minify_html($Html){
        $Search = array(
            '/(\n|^)(\x20+|\t)/',
            '/(\n|^)\/\/(.*?)(\n|$)/',
            '/\n/',
            '/\<\!--.*?-->/',
            '/(\x20+|\t)/', # Delete multispace (Without \n)
            '/\>\s+\</', # strip whitespaces between tags
            '/(\"|\')\s+\>/', # strip whitespaces between quotation ("') and end tags
            '/=\s+(\"|\')/'); # strip whitespaces between = "'
  
        $Replace = array(
            "\n",
            "\n",
            " ",
            "",
            " ",
            "><",
            "$1>",
            "=$1");
  
        $Html = preg_replace($Search, $Replace, $Html);
        return $Html;
    }

}