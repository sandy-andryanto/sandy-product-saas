<?php 

namespace App\Helpers;

use Illuminate\Http\Request;
use App\Models\Entities\Core\Attachment;
use Faker\Factory as Faker;

class UploadHelper {

    public static function uploadFile($model_type, $model_id, $file){
        if(isset($file)){  
            $target = "uploads";
			$faker = Faker::create();
            $uuid = $faker->uuid;
			$ext = $file->getClientOriginalExtension();
			$fileName = $uuid;
			$fileName = strtolower(md5($fileName).".".$ext);
			$destinationPath = public_path($target);
			$do_upload = $file->move($destinationPath, $fileName);
            if($do_upload){

                $ext = pathinfo($destinationPath."/".$fileName, PATHINFO_EXTENSION);
                $size = file_exists($destinationPath."/".$fileName) ? filesize($destinationPath."/".$fileName) : 0;
                $file_url = url($target."/".$fileName);

                $attachment = Attachment::getRowByModel($model_type, $model_id);
                if(!is_null($attachment)){
                    $attachment_path = $attachment->file_path;
                    if(file_exists($attachment_path)){
                        @unlink($attachment_path);
                    }
                    $update_data = Attachment::where("id", $attachment->id)->first();
                    $update_data->file_name = $fileName;
                    $update_data->file_type = $ext;
                    $update_data->file_size = $size;
                    $update_data->file_path = $destinationPath."/".$fileName;
                    $update_data->file_url = $file_url;
                    $update_data->save();

                    return $update_data;
                
                }else{

                    $formData = array(
                        'uuid'=> $uuid,
                        'model_id'=> $model_id,
                        'model_type'=> $model_type,
                        'file_name'=> $fileName,
                        'file_type'=> $ext,
                        'file_size'=> $size,
                        'file_path'=> $destinationPath."/".$fileName,
                        'file_url'=> $file_url
                    );
    
                    return Attachment::create($formData);

                }
            }
		}
        return null;
    }

    public static function uploadFiles($model_type, $model_id, $file_name){
        
    }

    public static function getFile($model_type, $model_id){
        $result = Attachment::getRowByModel($model_type, $model_id);
        return !is_null($result) ? $result->file_url : null;
    }

    public static function getFiles($model_type, $model_id){
        $data = array();
        $result = Attachment::getRowsByModel($model_type, $model_id);
        foreach($result as $row){
            $data[] = $row->file_url;
        }
        return $data;
    }

    public static function getFilesByModel($model_type){
        $data = array();
        $result = Attachment::where("model_type", $model_type)->get();
        foreach($result as $row){
            $data[] = $row->file_url;
        }
        return $data;
    }

    public static function getFilesById($uuid){
        $data = array();
        $result = Attachment::getRowsById($uuid);
        foreach($result as $row){
            $data[] = $result->file_url;
        }
        return $data;
    }

    public static function getFileById($uuid){
        $result = Attachment::getRowById($uuid);
        return !is_null($result) ? $result->file_url : null;
    }

}