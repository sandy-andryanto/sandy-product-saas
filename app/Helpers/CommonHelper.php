<?php 

namespace App\Helpers;

use Carbon\Carbon;
use Faker\Factory as Faker;
use App\Helpers\ConfigHelper;

class CommonHelper {

	public static function getMacAddress(){
		$MAC = exec('getmac');
		$MAC = strtok($MAC, ' ');
		return is_null($MAC) ? null : $MAC;
	}


    public static function dayNow(){
        $day_name = "";
		$day = date('l');

		if(slugify($day) == slugify('Monday')){
			$day_name = "Senin";
		}else if(slugify($day) == slugify('Tuesday')){
			$day_name = "Selasa";
		}else if(slugify($day) == slugify('Wednesday')){
			$day_name = "Rabu";
		}else if(slugify($day) == slugify('Thursday')){
			$day_name = "Kamis";
		}else if(slugify($day) == slugify('Friday')){
			$day_name = "Jumat";
		}else if(slugify($day) == slugify('Saturday')){
			$day_name = "Sabtu";
		}else if(slugify($day) == slugify('Sunday')){
			$day_name = "Minggu";
		}

		$month_name = "";
		$month = (int )date('m');

		if($month == 1){
			$month_name = "Januari";
		}else if($month == 2){
			$month_name = "Februari";
		}else if($month == 3){
			$month_name = "Maret";
		}else if($month == 4){
			$month_name = "April";
		}else if($month == 5){
			$month_name = "Mei";
		}else if($month == 6){
			$month_name = "Juni";
		}else if($month == 7){
			$month_name = "Juli";
		}else if($month == 8){
			$month_name = "Agustus";
		}else if($month == 9){
			$month_name = "September";
		}else if($month == 10){
			$month_name = "Oktober";
		}else if($month == 11){
			$month_name = "November";
		}elseif($month == 12){
			$month_name = "Desember";
		}

		return $day_name.", ".date('d')." ".$month_name." ".date("Y");
    }

    public static function minHTML($Html){
        $Search = array(
            '/(\n|^)(\x20+|\t)/',
            '/(\n|^)\/\/(.*?)(\n|$)/',
            '/\n/',
            '/\<\!--.*?-->/',
            '/(\x20+|\t)/', # Delete multispace (Without \n)
            '/\>\s+\</', # strip whitespaces between tags
            '/(\"|\')\s+\>/', # strip whitespaces between quotation ("') and end tags
            '/=\s+(\"|\')/'); # strip whitespaces between = "'
  
        $Replace = array(
            "\n",
            "\n",
            " ",
            "",
            " ",
            "><",
            "$1>",
            "=$1");
  
        $Html = preg_replace($Search, $Replace, $Html);
        return $Html;
    }

    public static function cleanString($val){
        $val = strtolower($val);
        $val = str_replace(" ", null, $val);
        $val = str_replace(".", null, $val);
        $val = str_replace("-", null, $val);
        $val = str_replace("/", null, $val);
        $val = str_replace("\\", null, $val);
        $val = str_replace("_", null, $val);
        $val = str_replace("(", null, $val);
        $val = str_replace(")", null, $val);
        return $val;   
    }

    public static function slugify($text){
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        $text = trim($text, '-');
        $text = preg_replace('~-+~', '-', $text);
        $text = strtolower($text);
        if (empty($text)) {
            return 'n-a';
        }
        return $text;
    }

    public static function genUUID(){
        $faker = Faker::create();
        return $faker->uuid;
    }

    public static function getClientIp(){
        $ip = "console";
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (!empty($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public static function getMonthName($index){
        if((int) $index == 1){
			return "Januari";
		}else if((int) $index == 2){
			return "Februari";
		}else if((int) $index == 3){
			return "Maret";
		}else if((int) $index == 4){
			return "April";
		}else if((int) $index == 5){
			return "Mei";
		}else if((int) $index == 6){
			return "Juni";
		}else if((int) $index == 7){
			return "Juli";
		}else if((int) $index == 8){
			return "Agustus";
		}else if((int) $index == 9){
			return "September";
		}else if((int) $index == 10){
			return "Oktober";
		}else if((int) $index == 11){
			return "November";
		}else if((int) $index == 12){
			return "Desember";
		}else{
			return "undefined";
		}
    }

}