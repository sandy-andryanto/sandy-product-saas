<?php 

namespace App\Helpers;

use App\Models\Entities\Auth\User;
use App\Models\Entities\Core\Attachment;
use App\Models\Core\AdminMenu;
use App\Models\Core\TablePermission;
use App\Helpers\CommonHelper;
use Carbon\Carbon;

class UserHelper {

    public static function getRealName(){

        $user = \Auth::User();
        $full_name = $user->full_name;
        $nick_name = $user->nick_name;

        if(strlen($nick_name) > 0){
            return $nick_name;
        }else if(strlen($full_name) > 0){
            return $full_name;
        }else{
            return $user->username;
        }
    }

    public static function getRealPhoto(){

        $user = \Auth::User();
        $gender = (int) $user->gender;

        $user_photo = Attachment::getRowByModel(User::class, $user->id);
        if(!is_null($user_photo)){
            $path = $user_photo->file_url;
            if(strlen($path) > 0){
                return url($path);
            }else{
                if($gender == 1){
                    return asset('app/img/male.png');
                }else if($gender == 2){
                    return asset('app/img/female.png');
                }else{
                    return asset('app/img/user.png');
                }
            }
        }else{
            if($gender == 1){
                return asset('app/img/male.png');
            }else if($gender == 2){
                return asset('app/img/female.png');
            }else{
                return asset('app/img/user.png');
            }
        }
    }

    public static function getRoles(){
        $user = \Auth::User();
        $roles = $user->Roles->pluck("name")->toArray();
        $arr = array();
        foreach($roles as $role){
            $arr[] = $role;
        }
        return implode(", ", $arr);
    }

    public static function getJoinDate(){
        $user = \Auth::User();
        $createdAt = Carbon::parse($user->created_at);
        return $createdAt->format('d/m/Y');
    }

    public static function getAdminMenu(){
        $user = \Auth::User();
        $user_id = $user->id;
        $html = AdminMenu::getMenu($user_id);
        return CommonHelper::minHTML($html);
    }

   public static function generateTablePermission(){
        return TablePermission::render();
    }

    public static function isDemo(){
        $user = \Auth::User();
        $company = $user->Company;
        if(!is_null($company->date_expired)){
            return true;
        }
        return false;
    }

    public static function DemoRemainingText(){
        $user = \Auth::User();
        $company = $user->Company;
        if(!is_null($company->date_expired)){
            $firstDate = Carbon::parse($user->created_at);
            $lastDate = Carbon::parse($user->company->date_expired);
            $remaining = $firstDate->diffInDays($lastDate);
            return strtoupper("masa demo aplikasi ini akan berakhir ".$remaining." hari lagi.");
        }
        return null;
    }

}