<?php 

namespace App\Helpers;

use App\Models\Entities\Core\Setting;

class ConfigHelper {

    public static function getValueByKey($key){
        $result = Setting::where("key_name", trim($key))->first();
        if($key == 'favicon'){
            if(!is_null($result)){
                $path = public_path($result->key_value);
                if(file_exists($path)){
                    return $result->key_value;
                }else{
                    return "app/img/logo.png";
                }
            }else{
                return "app/img/logo.png";
            }
        }else{
            return is_null($result) ? $key : $result->key_value;
        }
    }

}