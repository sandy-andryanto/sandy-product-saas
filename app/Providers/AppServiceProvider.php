<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::setLocale('id');

        Schema::defaultStringLength(191);

        $dir_upload = public_path("uploads");
        if(!is_dir($dir_upload)){
            @mkdir(public_path("uploads"));
        }

        $dir_files = public_path("files");
        if(!is_dir($dir_files)){
            @mkdir(public_path("files"));
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
