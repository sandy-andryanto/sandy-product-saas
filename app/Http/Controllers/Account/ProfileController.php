<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller as LaravelController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Models\Entities\Area\Country;
use App\Models\Entities\Area\Province;
use App\Models\Entities\Area\Regency;
use App\Models\Entities\Area\District;
use App\Models\Entities\Area\Village;
use App\Models\Entities\Reference\WorkType;
use App\Models\Entities\Reference\BloodType;
use App\Models\Entities\Auth\Person;
use App\Models\Entities\HRM\Employee;

/**
 * @Middleware("web")
 * @Middleware("xss")
 * @Middleware("auth")
 * @Middleware("timeout")
 * @Controller(prefix="account")
 */
class ProfileController extends LaravelController{

    private $data = array();


    /**
     * 
     * @Get("/profile", as="profile.index")
     */
    public function index(){
        $user = \Auth::User();
        $ip_address = $this->get_client_ip();
        return view('account.profile')->with('user', $user)->with('ip_address', $ip_address);
    }

    /**
     * 
     * @Post("/profile/update", as="profile.update")
     */
    public function update(Request $request){
        $user_id = \Auth::User()->id;
        $rules = [
            'username' => 'required|alpha_dash|unique:auth_users,username,' . $user_id,
            'email' => 'required|email|unique:auth_users,email,' . $user_id
        ];
        if($request->get('phone')) $rules["phone"] = 'required|regex:/^[0-9]+$/|unique:auth_users,phone,'.$user_id;
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }else{

            $user = \Auth::User();
            $user->username = $request->get("username");
            $user->email = $request->get("email");
            if($request->get("phone")){
                $user->phone = $request->get("phone");
            }
            $user->full_name = $request->get("full_name");
            $user->nick_name = $request->get("nick_name");
            $user->gender = $request->get("gender");
            $user->regency = $request->get("regency");
            $user->address = $request->get("address");
            $user->save();

           
            return redirect()->route('profile.index')->with('success', 'Profil pengguna berhasil diubah.');
        }
    }

    function get_client_ip(){
        $ip = "console";
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (!empty($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    
}
