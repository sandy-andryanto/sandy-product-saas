<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller as LaravelController;
use Illuminate\Http\Request;
use App\Models\Entities\Auth\Notification;
use Yajra\Datatables\Datatables;

/**
 * @Resource("notifications")
 * @Middleware("web")
 * @Middleware("auth")
 * @Middleware("xss")
 * @Middleware("timeout")
 * @Controller(prefix="account")
 */
class NotificationController extends LaravelController{

    protected $data = array();
    protected $title = "Pemberitahuan";
    protected $route_name = "notifications";
    protected $script = 'app/js/notifications.js';
    protected $view_source = "account.notifications";

    public function index(){
        $this->data["model"] = new Notification;
        $this->data["title"] = $this->title;
        $this->data["route"] = $this->route_name;
        $this->data["script"] = asset($this->script."?".time());
        return view($this->view_source.'.list', $this->data);
    }

    /**
     * 
     * @Post("/notifications/datatable", as="notifications.datatable")
     */
    public function datatable(Request $request){
        $user = \Auth::User();
        $result = Notification::where("user_id", $user->id)->orderBy("id", "DESC")->get();
        return Datatables::of($result)
            ->addColumn('action', function($result) use ($user){
                $route_view = route($this->route_name.".show", array("id"=> $result->id));
                $route_edit = route($this->route_name.".edit", array("id"=> $result->id));
                $route_delete = route($this->route_name.".destroy", array("id"=> $result->id));
                $action = "<a href='".$route_view."' class='btn btn-sm btn-info' data-toggle='tooltip' data-placement='top' title='Lihat Detail'><i class='fas fa-search'></i></a>";
                $action .= "&nbsp;<a href='".$route_delete."' class='btn btn-sm btn-danger btn-delete' data-id='".$result->id."' data-toggle='tooltip' data-placement='top' title='Hapus Data'><i class='fas fa-trash'></i></a>";
                return $action;
            })->make(true);
    }

    public function create(){
        return abort(404);
    }

    public function store(Request $request){
        return abort(404);
    }

    public function update($id, Request $request){
        return abort(404);
    }

    public function show($id){

        $user_id = \Auth::User()->id;
        $model = Notification::where("id", $id)->where("user_id", $user_id)->first();

        if(is_null($model)){
            return abort(404);
        }

        $model->readed_at = now();
        $model->save();

        $this->data["model"] = $model;
        $this->data["title"] = $this->title;
        $this->data["route"] = $this->route_name;
        $this->data["script"] = asset($this->script."?".time());
        return view($this->view_source.'.detail', $this->data);

    }

    public function edit($id){
        return abort(404);
    }

    public function destroy($id){

        $model = Notification::where("id", $id)->first();

        if(is_null($model)){
            return abort(404);
        }

        $response = Notification::where("id", $id)->delete();
        return response()->json($response);
    }
    
}