<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as LaravelController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route as RouteApp;
use App\Models\Traits\Authorizable;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;

class CrudController extends LaravelController{

    use Authorizable;

    protected $data = array();
    protected $view_source;
    protected $title;
    protected $route_name;
    protected $model;
    protected $script;

    public function index(){
        $this->data["title"] = $this->title;
        $this->data["route"] = $this->route_name;
        $this->data["script"] = asset($this->script."?".time());
        return view($this->view_source.'.list', $this->data);
    }
    
    public function create(){
        $this->data["model"] = new $this->model;
        $this->data["title"] = $this->title;
        $this->data["route"] = $this->route_name;
        $this->data["script"] = asset($this->script."?".time());
        return view($this->view_source.'.form', $this->data);
    }

    public function store(Request $request){

        $user = \Auth::User();
        $company_id = $user->company_id;

        $rules = $this->create_validation();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }else{
            $route = $this->route_name;
            $eloquent = new $this->model;
            $eloquent->company_id = $company_id;

            if($request->has("code")){
                $check_kode = $eloquent->where("code", $request->get("code"))->where("company_id", $company_id)->first();
                if(!is_null($check_kode)){
                    return back()->withErrors(['code' => ['Kode "'.$request->get("code").'" sudah digunakan!.']]);
                }
            }

            if($request->has("name")){
                $check_nama = $eloquent->where("name", $request->get("name"))->where("company_id", $company_id)->first();
                if(!is_null($check_nama)){
                    return back()->withErrors(['name' => ['Nama "'.$request->get("name").'" sudah digunakan!.']]);
                }
            }

            if($request->has("email")){
                $check_email = $eloquent->where("email", $request->get("email"))->where("company_id", $company_id)->first();
                if(!is_null($check_email)){
                    return back()->withErrors(['email' => ['Alamat email "'.$request->get("email").'" sudah digunakan!.']]);
                }
            }

            if($request->has("phone")){
                $check_phone = $eloquent->where("phone", $request->get("phone"))->where("company_id", $company_id)->first();
                if(!is_null($check_phone)){
                    return back()->withErrors(['phone' => ['Nomor telepon "'.$request->get("phone").'" sudah digunakan!.']]);
                }
            }


            $formData = $eloquent->getFillable();
            foreach($formData as $row){
                if($request->has($row)){
                    $eloquent->$row = $request->get($row);
                }
            }
            $eloquent->save();
            $id = $eloquent->id;
            $this->after_create($request, $request->all(), $eloquent);
            return redirect()->route($route.".show", ["id"=> $id])->with('success', "Data ".$this->title." berhasil disimpan!.");
        }
    }

    public function update($id, Request $request){

        $user = \Auth::User();
        $company_id = $user->company_id;

        if(is_null($id)){
            return abort(404);
        }

        $rules = $this->edit_validation($id);
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }else{
            $route = $this->route_name;
            $source = new $this->model;
            $eloquent = $source->where("id", $id)->first();
            $formData = $eloquent->getFillable();
            foreach($formData as $row){
                if($request->has($row)){
                    $eloquent->$row = $request->get($row);
                }
            }

            if($request->has("code")){
                $check_kode = $source->where("code", $request->get("code"))->where("company_id", $company_id)->where("id", "!=", $id)->first();
                if(!is_null($check_kode)){
                    return back()->withErrors(['code' => ['Kode "'.$request->get("code").'" sudah digunakan!.']]);
                }
            }

            if($request->has("name")){
                $check_nama = $source->where("name", $request->get("name"))->where("company_id", $company_id)->where("id", "!=", $id)->first();
                if(!is_null($check_nama)){
                    return back()->withErrors(['name' => ['Nama "'.$request->get("name").'" sudah digunakan!.']]);
                }
            }

            if($request->has("email")){
                $check_email = $source->where("email", $request->get("email"))->where("company_id", $company_id)->where("id", "!=", $id)->first();
                if(!is_null($check_email)){
                    return back()->withErrors(['email' => ['Alamat email "'.$request->get("email").'" sudah digunakan!.']]);
                }
            }

            if($request->has("phone")){
                $check_phone = $source->where("phone", $request->get("phone"))->where("company_id", $company_id)->where("id", "!=", $id)->first();
                if(!is_null($check_phone)){
                    return back()->withErrors(['phone' => ['Nomor telepon "'.$request->get("phone").'" sudah digunakan!.']]);
                }
            }

            $eloquent->save();
            $id = $eloquent->id;
            $this->after_edit($request, $request->all(), $eloquent, $id);
            return redirect()->route($route.".show", ["id"=> $id])->with('success', "Data ".$this->title." berhasil disimpan!.");
        }
    }

    public function show($id){

        $user = \Auth::User();
        $company_id = $user->company_id;

        if(is_null($id)){
            return abort(404);
        }

        $eloquent = new $this->model;
        $entitiy = $eloquent->where("id", $id)->first();

        if(is_null($entitiy)){
            return abort(404);
        }

        if($entitiy->company_id != $company_id){
            return abort(404);
        }

        $this->data["model"] = $entitiy;
        $this->data["title"] = $this->title;
        $this->data["route"] = $this->route_name;
        $this->data["script"] = asset($this->script."?".time());
        return view($this->view_source.'.detail', $this->data);

    }

    public function edit($id){

        $user = \Auth::User();
        $company_id = $user->company_id;

        if(is_null($id)){
            return abort(404);
        }

        $eloquent = new $this->model;
        $entitiy = $eloquent->where("id", $id)->first();

        if(is_null($entitiy)){
            return abort(404);
        }

        if($entitiy->company_id != $company_id){
            return abort(404);
        }

        $this->data["model"] = $entitiy;
        $this->data["title"] = $this->title;
        $this->data["route"] = $this->route_name;
        $this->data["script"] = asset($this->script."?".time());
        return view($this->view_source.'.form', $this->data);
    }

    public function destroy($id){

        if(is_null($id)){
            return abort(404);
        }

        $eloquent = new $this->model;
        $entitiy = $eloquent->where("id", $id)->first();

        if(is_null($entitiy)){
            return abort(404);
        }

        $response = $eloquent->where("id", $id)->delete();
        $this->after_delete($id);
        return response()->json($response);
    }

    public function datatable(Request $request){

        $user = \Auth::User();
        $company_id = $user->company_id;

        $eloquent = new $this->model;
        $result = $eloquent->where("company_id", $company_id)->orderBy("id", "DESC")->get();
        $user = \Auth::User();
        return Datatables::of($result)
            ->addColumn('action', function($result) use ($user){

                $route_view = route($this->route_name.".show", array("id"=> $result->id));
                $route_edit = route($this->route_name.".edit", array("id"=> $result->id));
                $route_delete = route($this->route_name.".destroy", array("id"=> $result->id));
                $action = "<a href='".$route_view."' class='btn btn-sm btn-info' data-toggle='tooltip' data-placement='top' title='Lihat Detail'><i class='fas fa-search'></i></a>";

                if($user->can("edit_".$this->route_name)){
                    $action .= "&nbsp;<a href='".$route_edit."' class='btn btn-sm btn-warning' data-toggle='tooltip' data-placement='top' title='Edit Data'><i class='fas fa-edit'></i></a>";
                }

                if($user->can("delete_".$this->route_name)){
                    $action .= "&nbsp;<a href='".$route_delete."' class='btn btn-sm btn-danger btn-delete' data-id='".$result->id."' data-toggle='tooltip' data-placement='top' title='Hapus Data'><i class='fas fa-trash'></i></a>";
                }

                return $action;
            })->make(true);
    }

    protected function create_validation(){
        return array();
    }

    protected function edit_validation($id){
        return array();
    }

    protected function after_create($request, $formData, $newData){

    }

    protected function after_edit($request, $formData, $newData, $id){
        
    }

    protected function after_delete($id){
        
    }
    
}
