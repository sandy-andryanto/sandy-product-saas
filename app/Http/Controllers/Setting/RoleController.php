<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\CrudController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;
use App\Helpers\CommonHelper;
// Load Models
use App\Models\Entities\Auth\Role;
use App\Models\Entities\Auth\Route;
use App\Models\Entities\Auth\Permission;
/**
 * @Resource("roles")
 * @Middleware("web")
 * @Middleware("auth")
 * @Middleware("xss")
 * @Middleware("timeout")
 * @Controller(prefix="configuration")
 */
class RoleController extends CrudController{

    protected $data = array();
    protected $view_source = "configuration.roles";
    protected $title = "Hak Akses";
    protected $route_name = "roles";
    protected $model = Role::class;
    protected $script = 'app/js/roles.js';
    protected $ignore = array("ADMIN", "CLIENT");

    /**
     * 
     * @Post("/roles/datatable", as="roles.datatable")
     */
     public function datatable(Request $request){
        $eloquent = new $this->model;
        $result = $eloquent->orderBy("id", "DESC")->get();
        $user = \Auth::User();
        $ignore = $this->ignore;
        return Datatables::of($result)
            ->addColumn('action', function($result) use ($user, $ignore){

                $route_view = route($this->route_name.".show", array("id"=> $result->id));
                $route_edit = route($this->route_name.".edit", array("id"=> $result->id));
                $route_delete = route($this->route_name.".destroy", array("id"=> $result->id));
                $action = "<a href='".$route_view."' class='btn btn-sm btn-info' data-toggle='tooltip' data-placement='top' title='Lihat Detail'><i class='fa fa-search'></i></a>";

                if($user->can("edit_".$this->route_name)){
                    $action .= "&nbsp;<a href='".$route_edit."' class='btn btn-sm btn-warning' data-toggle='tooltip' data-placement='top' title='Edit Data'><i class='fa fa-edit'></i></a>";
                }

                if(!in_array($result->name, $ignore)){
                    if($user->can("delete_".$this->route_name)){
                        $action .= "&nbsp;<a href='".$route_delete."' class='btn btn-sm btn-danger btn-delete' data-id='".$result->id."' data-toggle='tooltip' data-placement='top' title='Hapus Data'><i class='fa fa-trash'></i></a>";
                    }
                }
               

                return $action;
            })->make(true);
    }

    public function index(){
        $user = \Auth::User();
        $company = $user->Company;
        if(!is_null($company->date_expired)){
            return abort(403);
        }
        return parent::index();
    }

     public function create(){

        $user = \Auth::User();
        $company = $user->Company;
        if(!is_null($company->date_expired)){
            return abort(403);
        }

        $this->data["model"] = new $this->model;
        $this->data["title"] = $this->title;
        $this->data["route"] = $this->route_name;
        $this->data["script"] = asset($this->script."?".time());
        $this->data["ignore"] = false;
        $this->data["permission_data"] = array();
        return view($this->view_source.'.form', $this->data);
    }

    public function store(Request $request){
        $rules = array(
            'name' => 'required|unique:auth_roles',
            'routes' => 'required|min:1',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }else{
            
            $permission_id = array();
            $route_ids = $request->get("routes");
            $routes = Route::whereIn("id", $route_ids)->get();
            foreach($routes as $route){
                $route_id = $route->id;
                $route_name = $route->route;
                $route_name = str_replace(".index", "", $route_name);
                $route_access = ["view", "add", "edit", "delete"];
                foreach($route_access as $rs){
                    if($request->get("can_".$rs."".$route_id)){
                        $permission = Permission::where("name", $rs."_".$route_name)->first();
                        if(!is_null($permission)){
                            $permission_id[] = $permission->id;
                        }
                    }   
                }
            }
          
            $input_name = strtoupper($request->get("name"));
            $input_name = CommonHelper::slugify($input_name);
            $input_name = str_replace("-", "_", $input_name);

            $role = Role::firstOrCreate([
                "name"=> strtoupper($input_name),
                "description"=> $request->get("description")
            ]);
            $permission_selected = Permission::whereIn("id", $permission_id)->get();
            $role->syncPermissions($permission_selected);    
            $this->syncRoutes($role, $request->get("routes"));
            $id = $role->id;

            return redirect()->route("roles.show", ["id"=> $id])->with('success', "Data ".$this->title." berhasil disimpan!.");
        }
    }

    public function show($id){

        if(is_null($id)){
            return abort(404);
        }

        $user = \Auth::User();
        $company = $user->Company;
        if(!is_null($company->date_expired)){
            return abort(403);
        }

        $eloquent = new $this->model;
        $entitiy = $eloquent->where("id", $id)->first();

        if(is_null($entitiy)){
            return abort(404);
        }

        $routes = array();
        $role_permission = $entitiy->Permissions()->get();
        foreach($role_permission as $prm){
            $route_name = trim($prm->name);
            $route_name = str_replace("add_", "", $route_name);
            $route_name = str_replace("view_", "", $route_name);
            $route_name = str_replace("edit_", "", $route_name);
            $route_name = str_replace("delete_", "", $route_name);
            $route_name = $route_name.".index";
            $routes[] = $route_name;
        }
        $routes = array_unique($routes);
        $routes = array_values($routes);
        $route_data = Route::whereIn("route", $routes)->get();
        $permission = array();

        $i = 1;
        foreach($route_data as $r){
            $route_name = trim($r->route);
            $route_name = str_replace(".index", "", $route_name);
            $permission[] = array(
                "id"=> $i,
                "parent_id"=> $r->parent_id,
                "route_id"=> $r->id,
                "role_id"=> (int) $id,
                "can_view"=> Role::checkPermission($id, "view_".$route_name) ? 1 : 0,
                "can_edit"=> Role::checkPermission($id, "edit_".$route_name) ? 1 : 0,
                "can_create"=> Role::checkPermission($id, "add_".$route_name) ? 1 : 0,
                "can_delete"=> Role::checkPermission($id, "delete_".$route_name) ? 1 : 0,
            );
            $i++;
        }

        $this->data["model"] = $entitiy;
        $this->data["title"] = $this->title;
        $this->data["route"] = $this->route_name;
        $this->data["script"] = asset($this->script."?".time());
        $this->data["permission_data"] = $permission;
        $this->data["ignore"] = in_array($entitiy->name, $this->ignore);
        return view($this->view_source.'.detail', $this->data);

    }

     public function edit($id){

        if(is_null($id)){
            return abort(404);
        }

        $user = \Auth::User();
        $company = $user->Company;
        if(!is_null($company->date_expired)){
            return abort(403);
        }

        $eloquent = new $this->model;
        $entitiy = $eloquent->where("id", $id)->first();

        if(is_null($entitiy)){
            return abort(404);
        }

        $routes = array();
        $role_permission = $entitiy->Permissions()->get();
        foreach($role_permission as $prm){
            $route_name = trim($prm->name);
            $route_name = str_replace("add_", "", $route_name);
            $route_name = str_replace("view_", "", $route_name);
            $route_name = str_replace("edit_", "", $route_name);
            $route_name = str_replace("delete_", "", $route_name);
            $route_name = $route_name.".index";
            $routes[] = $route_name;
        }
        $routes = array_unique($routes);
        $routes = array_values($routes);
        $route_data = Route::whereIn("route", $routes)->get();
        $permission = array();

        $i = 1;
        foreach($route_data as $r){
            $route_name = trim($r->route);
            $route_name = str_replace(".index", "", $route_name);
            $permission[] = array(
                "id"=> $i,
                "parent_id"=> $r->parent_id,
                "route_id"=> $r->id,
                "role_id"=> (int) $id,
                "can_view"=> Role::checkPermission($id, "view_".$route_name) ? 1 : 0,
                "can_edit"=> Role::checkPermission($id, "edit_".$route_name) ? 1 : 0,
                "can_create"=> Role::checkPermission($id, "add_".$route_name) ? 1 : 0,
                "can_delete"=> Role::checkPermission($id, "delete_".$route_name) ? 1 : 0,
            );
            $i++;
        }

        $this->data["model"] = $entitiy;
        $this->data["title"] = $this->title;
        $this->data["route"] = $this->route_name;
        $this->data["script"] = asset($this->script."?".time());
        $this->data["ignore"] = in_array($entitiy->name, $this->ignore);
        $this->data["permission_data"] = $permission;
        return view($this->view_source.'.form', $this->data);
    }
    
    public function update($id, Request $request){
        $rules = array(
            'name' => 'required|unique:auth_roles,name,' . $id,
            'routes' => 'required|min:1',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }else{
            
            $permission_id = array();
            $route_ids = $request->get("routes");
            $routes = Route::whereIn("id", $route_ids)->get();
            foreach($routes as $route){
                $route_id = $route->id;
                $route_name = $route->route;
                $route_name = str_replace(".index", "", $route_name);
                $route_access = ["view", "add", "edit", "delete"];
                foreach($route_access as $rs){
                    if($request->get("can_".$rs."".$route_id)){
                        $permission = Permission::where("name", $rs."_".$route_name)->first();
                        if(!is_null($permission)){
                            $permission_id[] = $permission->id;
                        }
                    }   
                }
            }

            $input_name = strtoupper($request->get("name"));
            $input_name = CommonHelper::slugify($input_name);
            $input_name = str_replace("-", "_", $input_name);
          
            $role = Role::where("id", $id)->first();
            $role->name = strtoupper($input_name);
            $role->description = $request->get("description");
            $role->save();

            $permission_selected = Permission::whereIn("id", $permission_id)->get();
            $role->syncPermissions($permission_selected);
            $this->syncRoutes($role, $request->get("routes"));

            return redirect()->route("roles.show", ["id"=> $id])->with('success', "Data ".$this->title." berhasil diupdate!.");
        }
    }

     private function syncRoutes($role, array $routes){
        $route_id = array();
        $routes = Route::whereIn("id", $routes)->get();
        foreach($routes as $route){
            $route_id[] = $route->id;
            if(!is_null($route->parent_id)){
                $route_id[] = $route->parent_id;
            }
        }
        $role->Routes()->sync($route_id);
    }

}