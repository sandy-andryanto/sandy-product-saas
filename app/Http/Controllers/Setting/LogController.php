<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\CrudController;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
// Load Models
use App\Models\Entities\Core\Log as LogApp;

/**
 * @Resource("logs")
 * @Middleware("web")
 * @Middleware("auth")
 * @Middleware("xss")
 * @Middleware("timeout")
 * @Controller(prefix="configuration")
 */
class LogController extends CrudController{

    protected $data = array();
    protected $view_source = "configuration.logs";
    protected $title = "Log Data";
    protected $route_name = "logs";
    protected $model = LogApp::class;
    protected $script = 'app/js/logs.js';

    /**
     * 
     * @Post("/logs/datatable", as="logs.datatable")
     */
    public function datatable(Request $request){
        $eloquent = new $this->model;
        $result = $eloquent
            ->orderBy("id", "DESC")
            ->get();
        return Datatables::of($result)->make(true);
    }

    public function index(){
        $user = \Auth::User();
        $company = $user->Company;
        if(!is_null($company->date_expired)){
            return abort(403);
        }
        return parent::index();
    }

    public function create(){
        return abort(404);
    }

    public function store(Request $request){
        return abort(404);
    }

    public function update($id, Request $request){
        return abort(404);
    }

    public function show($id){
        return abort(404);
    }

    public function edit($id){
        return abort(404);
    }

    public function destroy($id){
        return abort(404);
    }
    
}