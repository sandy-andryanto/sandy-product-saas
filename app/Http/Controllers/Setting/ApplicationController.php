<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\CrudController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Yajra\Datatables\Datatables;
// Load Models
use App\Models\Entities\Core\Setting;
use App\Models\Entities\Utils\TimeZone;
use App\Models\Entities\Utils\Currency;

/**
 * @Resource("settings")
 * @Middleware("web")
 * @Middleware("auth")
 * @Middleware("xss")
 * @Middleware("timeout")
 * @Controller(prefix="configuration")
 */
class ApplicationController extends CrudController{

    protected $data = array();
    protected $view_source = "configuration.settings";
    protected $title = "Pengaturan Umum";
    protected $route_name = "settings";
    protected $model = Setting::class;
    protected $script = 'app/js/settings.js';

    public function index(){

        $user = \Auth::User();
        $company = $user->Company;
        if(!is_null($company->date_expired)){
            return abort(403);
        }

        $this->data["model"] = new Setting;
        $this->data["title"] = $this->title;
        $this->data["route"] = $this->route_name;
        $this->data["timezones"] = TimeZone::getData();
        $this->data["currencies"] = Currency::getData();
        $this->data["script"] = asset($this->script."?".time());
        return view($this->view_source.'.list', $this->data);
    }

    /**
    * 
    * @Post("/settings/artisan", as="settings.artisan")
    */
    public function artisan_update(Request $request){
        $command = $request->get("command");
        $result = Artisan::call($command);
        return response()->json($result);
     }
    
    public function create(){
        return abort(404);
    }

    public function store(Request $request){
        
        $data = $request->all();
        foreach($data as $row => $key){
            $slug_name = $row;
            $key_value = $key;
            if($slug_name != '_token'){
                $model = Setting::where("key_name", $slug_name)->first();
                if(!is_null($model)){
                    $model->key_value = $key_value;
                    $model->save();
                }else{
                    Setting::create([
                        'key_name'=> $slug_name,
                        'key_value'=> $key_value
                    ]);
                }
            }
        }

        if($request->file('favicon')){
            $file_image =  $this->upload_image($request, 'favicon');
            $model = Setting::where("key_name", 'favicon')->first();
            if(!is_null($model)){
                $current_image = $model->key_value;
                if($current_image != 'app/img/logo.png'){
                    if(file_exists(public_path($current_image))){
                        unlink(@public_path($current_image));
                    }
                }
                $model->key_value = $file_image;
                $model->save();
            }
        }

        return redirect()->route("settings.index")->with('success', " ".$this->title." berhasil disimpan!.");

    }

    public function update($id, Request $request){
        return abort(404);
    }

    public function show($id){
        return abort(404);
    }

    public function edit($id){
        return abort(404);
    }

    public function destroy($id){
        return abort(404);
    }

    public function upload_image(Request $request, $name){
        $image = $request->file($name);
        if($image){
            $imageName = md5(time()."|".uniqid()).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads');
            $image->move($destinationPath, $imageName);
            return "uploads/".$imageName;
        }
        return null;
    }
    
}