<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\CrudController;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
// Load Models
use App\Models\Entities\Auth\Audit;

/**
 * @Resource("audits")
 * @Middleware("web")
 * @Middleware("auth")
 * @Middleware("xss")
 * @Middleware("timeout")
 * @Controller(prefix="configuration")
 */
class AuditController extends CrudController{

    protected $data = array();
    protected $view_source = "configuration.audits";
    protected $title = "Audit Trail";
    protected $route_name = "audits";
    protected $model = Audit::class;
    protected $script = 'app/js/audits.js';

    /**
     * 
     * @Post("/audits/datatable", as="audits.datatable")
     */
    public function datatable(Request $request){
        $user = \Auth::User();
        $company_id = $user->company_id;
        $eloquent = new $this->model;
        $result = $eloquent
            ->select(array(
                "auth_audits.id",
                "auth_audits.updated_at",
                "auth_users.username",
                "auth_audits.event",
                "auth_audits.url",
                "auth_audits.ip_address"
            ))
            ->orderBy("id", "DESC")
            ->where("auth_users.company_id", $company_id)
            ->join("auth_users", "auth_users.id", "auth_audits.user_id")
            ->get();
        return Datatables::of($result)->make(true);
    }

    public function create(){
        return abort(404);
    }

    public function store(Request $request){
        return abort(404);
    }

    public function update($id, Request $request){
        return abort(404);
    }

    public function show($id){
        return abort(404);
    }

    public function edit($id){
        return abort(404);
    }

    public function destroy($id){
        return abort(404);
    }
    
}