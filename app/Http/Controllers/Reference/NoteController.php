<?php

namespace App\Http\Controllers\Reference;

use App\Http\Controllers\CrudController;
use Illuminate\Http\Request;
// Load Models
use App\Models\Entities\Reference\Note;

/**
 * @Resource("notes")
 * @Middleware("web")
 * @Middleware("auth")
 * @Middleware("xss")
 * @Middleware("timeout")
 * @Controller(prefix="reference")
 */
class NoteController extends CrudController{

    protected $data = array();
    protected $view_source = "reference.notes";
    protected $title = "Catatan";
    protected $route_name = "notes";
    protected $model = Note::class;
    protected $script = 'app/js/notes.js';

    /**
     * 
     * @Post("/notes/datatable", as="notes.datatable")
     */
    public function datatable(Request $request){
        return parent::datatable($request);
    }

    protected function create_validation(){
        return array(
            'title' => 'required|unique:ref_notes',
            'body' => 'required'
        );
    }

    protected function edit_validation($id){
        return array(
            'title' => 'required|unique:ref_notes',
            'body' => 'required'
        );
    }
    
}