<?php

namespace App\Http\Controllers\Reference;

use App\Http\Controllers\CrudController;
use Illuminate\Http\Request;
// Load Models
use App\Models\Entities\Reference\Contact;

/**
 * @Resource("contacts")
 * @Middleware("web")
 * @Middleware("auth")
 * @Middleware("xss")
 * @Middleware("timeout")
 * @Controller(prefix="reference")
 */
class ContactController extends CrudController{

    protected $data = array();
    protected $view_source = "reference.contacts";
    protected $title = "Kontak Person";
    protected $route_name = "contacts";
    protected $model = Contact::class;
    protected $script = 'app/js/contacts.js';

    /**
     * 
     * @Post("/contacts/datatable", as="contacts.datatable")
     */
    public function datatable(Request $request){
        return parent::datatable($request);
    }

    protected function create_validation(){
        return array(
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
        );
    }

    protected function edit_validation($id){
        return array(
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
        );
    }
    
}