<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller as LaravelController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

/**
 * @Middleware("web")
 * @Middleware("xss")
 * @Middleware("auth")
 * @Middleware("timeout")
 * @Controller(prefix="dashboard")
 */
class SummaryController extends LaravelController{

    /**
     * 
     * @Get("/summary", as="dashboards.summary")
     */
    public function index(){
       return view('dashboard.summary');
    }

    
    
}
