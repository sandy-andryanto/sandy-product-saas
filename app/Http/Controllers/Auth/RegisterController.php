<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;
use Jrean\UserVerification\Facades\UserVerification as UserVerificationFacade;
use Jrean\UserVerification\Exceptions\UserNotFoundException;
use Jrean\UserVerification\Exceptions\UserIsVerifiedException;
use Jrean\UserVerification\Exceptions\TokenMismatchException;
use Illuminate\Auth\Events\Registered;
use App\Models\Entities\Auth\User;
use App\Models\Entities\Auth\Role;
use App\Models\Entities\Auth\Company;
use Faker\Factory as Faker;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers, VerifiesUsers;

    private $userTable = "auth_users";

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getVerification', 'getVerificationError']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|alpha_dash|unique:auth_users',
            'email' => 'required|string|email|max:255|unique:auth_users',
            'password' => 'required|string|min:6|confirmed',
            'captcha'=> 'required|captcha'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $roleUser = Role::where("name", trim("ADMIN"))->first();
        if(!is_null($roleUser)){

            $faker = Faker::create("id_ID");
            $company = Company::create([
                'name'=> $faker->company,
                'email'=> $faker->safeEmail,
                'phone'=> $faker->phoneNumber,
                'website'=> $faker->safeEmailDomain,
                "address"=> $faker->streetAddress,
                'date_expired'=> date('Y-m-d', strtotime("+30 days"))
            ]);

            $user = User::create([
                "company_id"=> $company->id,
                "username"=> $data["username"],
                "full_name"=> $data["username"],
                "nick_name"=> $data["username"],
                "email"=> $data["email"],
                "password"=>bcrypt($data["password"]),
                "is_admin"=> 1,
                'remember_token'=> base64_encode($data["username"]),
                'verified'=> 1,
                'verification_token'=> base64_encode($data["email"])
            ]);
            $user->assignRole($roleUser->name);

            for($i = 1; $i <=4; $i++){

                $dummy_username = $data["username"]."".rand(1000, 9999);
                $check_username = User::where("username", $dummy_username)->first();
                if(is_null($check_username)){
                    $dummy_email = $dummy_username."@testing.com";
                    $user = User::create([
                        "company_id"=> $company->id,
                        "username"=> $dummy_username,
                        "full_name"=> $dummy_username,
                        "nick_name"=> $dummy_username,
                        "email"=> $dummy_email,
                        "password"=>bcrypt($data["password"]),
                        "is_admin"=> 1,
                        'remember_token'=> base64_encode($dummy_username),
                        'verified'=> 1,
                        'verification_token'=> base64_encode($dummy_email)
                    ]);
                    $user->assignRole($roleUser->name);
                }

            }

            return $user;
        }
        return null;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = $this->create($request->all());
        event(new Registered($user));
        $this->guard()->logout();
        return redirect()->route('login')->with(["success"=> "Akun anda berhasil dibuat. Sekarang anda sudah bisa login."]);
    }

    public function getVerification(Request $request, $token)
    {
        if (! $this->validateRequest($request)) {
            return redirect()->route('login')->with(["warning"=> "Mohon maaf email anda tidak dapat di identifikasi."]);
        }

        try {
            $user = UserVerificationFacade::process($request->input('email'), $token, $this->userTable());
        } catch (UserNotFoundException $e) {
            return redirect()->route('login')->with(["warning"=> "Mohon maaf email anda tidak dapat di identifikasi."]);
        } catch (UserIsVerifiedException $e) {
            return redirect()->route('login')->with(["info"=> "Email anda sudah di verifikasi sebelumnya. Sekarang anda sudah bisa login."]);
        } catch (TokenMismatchException $e) {
            return redirect()->route('login')->with(["warning"=> "Mohon maaf email anda tidak dapat di identifikasi."]);
        }

        return redirect()->route('login')->with(["success"=> "Email anda sudah di verifikasi. Sekarang anda sudah bisa login."]);
    }
}
