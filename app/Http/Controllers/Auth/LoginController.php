<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Carbon\Carbon;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function credentials(Request $request) {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL) ? $this->username() : 'username';
        return [
            $field => $request->get($this->username()),
            'password' => $request->password,
        ];
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
            'captcha'=> 'required|captcha'
        ]);
    }

    protected function authenticated(Request $request, $user) {

        if (!$user->verified) {
            auth()->logout();
            return back()->with('warning', 'Silahkan anda mengkonfirmasi akun yang sudah dibuat. sistem kita telah mengirim tautan verifikasi pada alamat email anda, silahkan periksa e-mail anda.');
        }

        if ($user->is_banned == 1) {
            auth()->logout();
            return back()->with('warning', 'Akun anda telah terblokir. Silahkan hubungi admin untuk informasi lebih lengkap.');
        }

        if(!is_null($user->Company->date_expired)){
            if (Carbon::today()->toDateString() >= $user->Company->date_expired) {
                auth()->logout();
                return redirect('login')->with('warning', 'Masa aktif anda telah habis pada aplikasi ini, Terima kasih telah menggunakan layanan ini.');
            }
        }

        $previous_session = $user->session_id;
        if ($previous_session) {
            \Session::getHandler()->destroy($previous_session);
        }

        \Auth::user()->session_id = \Session::getId();
        \Auth::user()->save();

        return redirect()->intended($this->redirectPath());

    }
}
