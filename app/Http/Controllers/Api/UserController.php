<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller as LaravelController;
use Illuminate\Http\Request;
use App\Utils\CropImage;
use App\Models\Entities\Auth\User;
use App\Models\Entities\Core\Attachment;
use Faker\Factory as Faker;
use App\Models\Entities\Auth\Notification;

/**
 * @Middleware("api")
 * @Middleware("xss")
 * @Middleware("auth:api")
 * @Controller(prefix="api/user")
 */
class UserController extends LaravelController{


    /**
     * 
     * @Post("/notifications/list", as="api.user.notifications")
     */
    public function notification(Request $request){
        $user = \Auth::User();
        $unread = Notification::where("user_id", $user->id)->where("readed_at", null)->count();
        $list = Notification::where("user_id", $user->id)->where("readed_at", null)->orderBy("id", "DESC")->take(5)->get();
        $response = array(
            "unread"=> $unread,
            "list"=> $list
        );
        return response()->json($response);
    }

    /**
     * 
     * @Post("/update/profile/image", as="api.user.update.image")
     */
    public function updateProfileImage(Request $request){

        $avatarSrc = isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null;
        $avatarData = isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null;
        $avatarFile = isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null;
        $crop = new CropImage($avatarSrc, $avatarData, $avatarFile);
    
        $path = $crop->getResult();
        $file_name = basename($path);
        $name = "Foto Profil";
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $size = filesize($path);
        $data = addslashes($path);
        $file_path = null;

        $user = \Auth::User();
        // Hapus sebelumnya

        $id = $user->id;
        $model = User::class;
        $current = Attachment::getRowByModel($model, $id);
        if(!is_null($current)){
            $file_path_current = $current->file_path;
            if(strlen($file_path_current) > 0){
                if(file_exists($file_path_current)){
                    @unlink($file_path_current);
                }
            }
        }else{
            $faker = Faker::create();
            Attachment::create([
                'uuid'=> $faker->uuid,
                'model_id'=> $id,
                'model_type'=> $model
            ]);
        }
        

        $temp = $path;
        $dst = public_path("uploads/".$file_name);
        $copy = @copy($temp, $dst);
        if($copy){
            $file_path = "uploads/".$file_name;
            if(file_exists($crop->getResult())){
                @unlink($crop->getResult());
            }

            if(file_exists($crop->getOriginal())){
                @unlink($crop->getOriginal());    
            }

            // Simpan Data Sekarang
            $file_photo = Attachment::getRowByModel($model, $id);
            $file_photo->file_name = "Foto Profil";
            $file_photo->file_type = $type;
            $file_photo->file_size = $size;
            $file_photo->file_path = public_path($file_path);
            $file_photo->file_url =  $file_path;
            $file_photo->save();

        }

        $response = array(
            'state' => 200,
            'message' => $crop->getMsg(),
            'result' => url($file_path)
        );
        return response()->json($response);
    }

    
    
}
