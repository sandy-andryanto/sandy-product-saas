<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Seeds\DeleteExpired;

class DeleteExpiredCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth:delete-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete data expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DeleteExpired::init();
    }
}
